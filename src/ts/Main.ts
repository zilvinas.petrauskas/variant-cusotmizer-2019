
let CalamigosMain;
let isMobile;

window.addEventListener('load', ()=>{
	CalamigosMain = Main.getInstance();
});


// check if it's on mobile
if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
    isMobile = true;
} else {
	isMobile = false;
}

import { SceneManager } from 'view/SceneManager';
import { Product } from './Product';
import { LOCALHOST_FALLBACK, EXTERNAL_ASSETS_URL, USE_EXTERNAL_ASSETS } from './view/constants';

declare const BASE_URL: string; // comming from shopify
	
export class Main
{
	public static instance: Main;
	public static getInstance(): Main
	{
		return Main.instance || new Main();
	}

	public _isMobile = isMobile;
	public _scene: SceneManager;

	private _optionsWrapper: any;

	private _initiated = false;
	private _busy = false;

	private elementControlsParent;

	private elementCustomizerParent;

	public _baseUrl: string;
	public product: Product;

	private textures;
	private _isLocalhost : boolean;
	private mobileOptionsVisible = false;
	public silentLoaderEnabled = true;

	// TODO - need to load Main silently on each page, so that it could check colors, take screenshot quickly when adding to cart
	// modify shopify's add to cart btn to get screenshot first from external server and only then trigger add to cart
	// whats the minimum list of stuff I need to preload silently?
	// 

	constructor(){

		Main.instance = this;

		this.product = new Product(this); // percfectly in future product would be loaded here, and UI triggers color change with events
		
		this._baseUrl = typeof BASE_URL !== 'undefined' ? BASE_URL : LOCALHOST_FALLBACK.baseURL;

		this._isLocalhost = this.isLocalhost();
		this.textures = {};

		this.toggleFormButtons(true);

		// console.log('-- IS SWEATER?', this.product.isSweater() );
		// console.log('-- IS BLANKET?', this.product.isBlanket() );

		if(this.product.isSweater()){
			
			this.textures.FRT = this.product.getItemTexture('FRT');
			this.textures.BCK = this.product.getItemTexture('BCK');
			this.textures.SLV = this.product.getItemTexture('SLV');

			this.textures.FRT_NORMAL = this.product.getItemNormalTexture('FRT');
			this.textures.BCK_NORMAL = this.product.getItemNormalTexture('BCK');
			this.textures.SLV_NORMAL = this.product.getItemNormalTexture('SLV');

		}else{

			this.textures.BLANKET = this.product.getItemTexture('BLANKET');
			this.textures.BLANKET_NORMAL = this.product.getItemNormalTexture('BLANKET');
			// console.log('blanket textures?', this.textures);
		}
		
		this.initUI();

		// preload textures early and make sure scene is ready but not rendered (to save resoruces), so that screenshot is taken super quick
		if(this.silentLoaderEnabled) this.silentLoad();

		// global events
		// can be triggered like this: window.dispatchEvent(new Event('customizer:show'));
		// can keep all code in customizer this way

		let busy = false; // prevent multiclicks on show-customizer button

		// EVENT SHOW CUSTOMIZER

		// WHAT IF things didnt finish starting and loading, but user clicks cusotmizer button - loader whouls be visible and dissapear after
		window.addEventListener('customizer:show', ()=>{

			// console.log('customizer:show - busy?', busy);

			if(busy) return;

			if( ! this._initiated ){
			
				// initiate first then display
				busy = true;
				// console.log('--- start');
				this.start();

			}else{

				// console.log('--- show');

				// show
				busy = true;
				this.toggle(true); // show left side
				this._scene.controlsVisibilityToggle(true); // show right side

				if(this.silentLoaderEnabled) this._scene.startRenderer();
			}

		});

		// EVENT HIDE CUSTOMIZER
		window.addEventListener('customizer:hide', ()=>{

			busy = false;
			this.hide();

			if(this.silentLoaderEnabled) this._scene.stopRenderer();
		});


		
	
		
	}

	private silentLoad(){

		// console.log('silent loading..');
		this._scene = new SceneManager(this);
		this._scene.$controlsParent = this.elementControlsParent;
		this._scene.$customizerParent = this.elementCustomizerParent;

	}

	private loadDefaultNormalMap__NOT_NEEDED(section){

		// uses default normal map in localhost folder
		// adds '_normal' just before '.png' 
		const insertBefore = '.png';
		const insertString = '_normal';
		const normalMapFile = LOCALHOST_FALLBACK[section];
		const index = normalMapFile.indexOf(insertBefore);
		const newFileName = normalMapFile.slice(0,index) + insertString + insertBefore;
		return newFileName;
	}

	public start(){
		
		document.getElementById('section-product').classList.add('customizer-started');

		if( ! this.silentLoaderEnabled ){ 
			// loading of these things are optional if we preload silently on page load and not on customizer's button click
			this._scene = new SceneManager(this);
			this._scene.$controlsParent = this.elementControlsParent;
			this._scene.$customizerParent = this.elementCustomizerParent;
		}else{

			// TODO - if progress bar not finished, queue this visibility change once its done
			this._scene.queueVisibilityChange();
			this._scene.startRenderer();
		}
		
		this.toggle(true);
		this._initiated = true;

		// console.log('got scene ready?', this._scene);

	}

	public hide(){
		this.toggle(false);
		this._scene.controlsVisibilityToggle(false);
	}

	// toggle visibility of injected customizer's canvas
	public toggle(visible: boolean){

		const visibleClassName = 'show';
	
		const canvasWrapper = document.querySelectorAll('#canvasWrapper')[0];

		if(visible && ! canvasWrapper.classList.contains(visibleClassName) ){
			canvasWrapper.classList.add(visibleClassName);
		}else if( ! visible && canvasWrapper.classList.contains(visibleClassName)){
			canvasWrapper.classList.remove(visibleClassName);
		}

	}

	public getBaseUrl(){
		return this._baseUrl || '';
	}

	public getAssetsUrl(){

		let url;
		
		if(USE_EXTERNAL_ASSETS === true && this._isLocalhost === false ){
			url = EXTERNAL_ASSETS_URL;
		}else{
			url = this.getBaseUrl();
		}
		return url;
	}

	public isLocalhost(){
        return window.location.hostname === "localhost" || window.location.hostname === "127.0.0.1";
	}
	
	public getTextureUrl(name){

		let url;
		
		if(this._isLocalhost === true){
			url = LOCALHOST_FALLBACK.TEXTURES_FOLDER;
		}else{
			url = this.getAssetsUrl();
		}

		if(this.textures[name]) return url+''+this.textures[name];

		console.warn('Texture name not found for', name, this.textures);
		return '';
	}

	private cloneProgressBar(id){

		// const originalProgressBar = document.querySelectorAll('#customizerProgressBar')[0];
		const originalProgressBar = document.querySelectorAll(`#${id}`)[0];
		const progressBarClone = originalProgressBar.cloneNode(true);

		// remove old progress bar from html
		while(originalProgressBar.firstChild) originalProgressBar.removeChild(originalProgressBar.firstChild);
		originalProgressBar.parentNode.removeChild(originalProgressBar); // ie fix
		// originalProgressBar.remove();

		return progressBarClone;
	}

	private initUI()
	{

		history.scrollRestoration = "manual"; // prevent ios scroll refresh when customizer is on

		// for customizer's css to work properly, add .playground to parent
		const $productSection = document.querySelectorAll('#section-product')[0];
		$productSection.classList.add('playGround');

		// put canvas into this
		const progressBarParent = document.querySelectorAll('#section-product div.site-box:nth-child(1) > div:nth-child(1)')[0];

		const outroBarClone = this.cloneProgressBar('customizerOutro');
		progressBarParent.insertBefore(outroBarClone, progressBarParent.firstChild);

		const progressBarClone = this.cloneProgressBar('customizerProgressBar');
		progressBarParent.insertBefore(progressBarClone, progressBarParent.firstChild);

	


		this.elementCustomizerParent = progressBarParent.parentElement.parentElement;

		const optionsToggler = document.querySelectorAll('#optionsToggler')[0];
		const arrow = document.querySelectorAll('#arrow')[0];
		if(optionsToggler){
			optionsToggler.addEventListener('click', (e)=>{
				e.preventDefault();
				this.changeOptionsVisibility(arrow);
			});

			// const detectSwipeDown = new MobileGestures(document.getElementById('optionsWrapper'), ()=>{
			// 	if(this.mobileOptionsVisible) this.changeOptionsVisibility(arrow);
			// } );

		}

		// button 'cancel'
		const btnCancel = document.querySelectorAll('.customizerCancel');
		if(btnCancel){
			for(let i=0;i<btnCancel.length;i++){
				btnCancel[i].addEventListener('click', ()=>{
					window.dispatchEvent(new Event('customizer:hide'));
				});
			}
		}

		// buttons add to cart
		const addToCart = document.querySelectorAll('.customizerAddToCart');
		if(addToCart){
			for(let i=0;i<addToCart.length;i++){
				addToCart[i].addEventListener('click', (e)=>{
					console.log('product - add to cart button clicked');
					window.dispatchEvent(new Event('cart:add')); // check OrderManager.ts
				});
			}
		}

		// fake add to cart btn
		const fakeAddToCart = document.querySelectorAll('.customizer-add-cart');
		if(fakeAddToCart){
			for(let i=0;i<fakeAddToCart.length;i++){
				fakeAddToCart[i].addEventListener('click', (e)=>{
					console.log('product - add to cart button#2 clicked');
					window.dispatchEvent(new Event('cart:add')); // check OrderManager.ts
				});
			}
		}

		// test
		// testOutro
		const testOutro = document.querySelectorAll('.testOutro');
		if(testOutro){
			for(let i=0;i<testOutro.length;i++){
				testOutro[i].addEventListener('click', (e)=>{
					window.dispatchEvent(new Event('outro:start')); // check OrderManager.ts
				});
			}
		}

		// (function(){
		// 	let customCartRemoveAction = document.querySelectorAll('#site-cart .remove');
        //     if(customCartRemoveAction.length > 0){
        //         for(let i=0;i<customCartRemoveAction.length;i++){
        //             customCartRemoveAction[i].addEventListener('click', (e)=>{
        //                 console.log('cart remove', e);
        //                 window.dispatchEvent(new Event('cart:remove', e)); 
        //             });
        //         }
        //     }
		// });

		// const hollowClicks = document.querySelectorAll('.custom-header-click');
		// if(hollowClicks){
		// 	for(let i=0;i<hollowClicks.length;i++){
		// 		hollowClicks[i].addEventListener('click', (e:any)=>{
		// 			console.log('hollow click', e);
		// 			const elemBelow = document.querySelectorAll(e.target.dataset.trigger)[0];
		// 			if(elemBelow){
		// 				var evt = new MouseEvent('click', {view: window});
		// 				elemBelow.dispatchEvent(evt);
		// 			}
					
		// 		} );
		// 	}
		// }
		

		// for testing
		const btnTakeShot = document.querySelectorAll('#take-screenshot')[0];
		if(btnTakeShot){

			btnTakeShot.addEventListener('click', ()=>{

				// Main.instance._scene.triggerRender();

				let testParent = document.getElementsByTagName('body')[0];
				// let testCanvas:any = document.getElementById('myCanvas');
				// const screenshot = this.getProductScreenshot();
				// const prntscrn = this.product.screenshot();
				const prntscrn = this.product.perfectScreenshot();
				const screenshot = prntscrn.image;
				let testImg = document.createElement("img"); 
				testImg.classList.add('temp-image');
				testImg.src = screenshot;
				testImg.addEventListener('click', ()=>{
					testParent.removeChild(testImg);
				})
				testParent.appendChild(testImg);



			})

		
		}

		
		window.addEventListener('keyup', (e)=>{
			console.log(e.keyCode);
			if(e.keyCode === 67){ // c
				console.log('camera position', Main.instance.scene.camera.position);
			}else if(e.keyCode === 86){

				Main.instance.scene.camera.updateMatrixWorld();
				var vector = Main.instance.scene.camera.position.clone();
				vector.applyMatrix4( Main.instance.scene.camera.matrixWorld );

				console.log('camera', vector, Main.instance.scene.camera.position);
			}
		});



		// enable buttons again, things should be ready by now
		setTimeout(()=>{
			this.toggleFormButtons(false);
		}, 333);
	}

	private toggleFormButtons(disabled: boolean = false){
		const buttons = document.querySelectorAll('.cart-functions button');
		if(buttons && buttons.length > 0){
			for(let i=0;i<buttons.length;i++){
				if(disabled === true){
					buttons[i].setAttribute("disabled", "disabled");
				}else{
					buttons[i].removeAttribute("disabled");
				}
			}
		}
	}

	public getProductScreenshot(){

		
		Main.instance._scene.triggerRender();
		const canvas:any = document.getElementById('myCanvas');
		
		/* fit canvas into these bounds */
		const max = {
			width: 200,
			height: 200
		}		  		
		let canvasCopy = document.createElement("canvas")
		const copyContext = canvasCopy.getContext("2d")
		var resized:any = {};

		if(canvas.width / canvas.height > max.width / max.height) { 
			resized.width = max.width;
			resized.height = (canvas.height * (max.width / canvas.width)).toFixed(0);
		}else { 
			resized.width = (canvas.width * (max.height / canvas.height)).toFixed(0);
			resized.height = max.height;
		}

		canvasCopy.width = resized.width
		canvasCopy.height = resized.height;
		copyContext.drawImage(canvas, 0, 0, canvas.width, canvas.height, 0, 0, canvasCopy.width, canvasCopy.height);

		const screenshot = canvasCopy.toDataURL("image/png"); // WITH resize return resized canvas image
		/* end of resize */

		// const screenshot = canvas.toDataURL("image/png"); // without resize return original canvas

		return screenshot
	}

	private changeOptionsVisibility(arrow){
		arrow.classList.toggle('up');
		arrow.classList.toggle('down');
		document.getElementById('optionsWrapper').classList.toggle('mobile-visible');
		this.mobileOptionsVisible = !this.mobileOptionsVisible;
	}

	public get scene(): SceneManager
	{
		return this._scene;
	}

}

