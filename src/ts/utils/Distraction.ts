import { ProgressBar } from "./ProgressBar";
import { SceneManager } from 'view/SceneManager';

export class Distraction extends ProgressBar{

    // texts change while outro is loading
    private texts = {
        0 : 'Creating Your Garment',
        60: 'Applying Colors',
        // 80: 'Almost Done',
    }

    private runTimes = 0; // if >0 then trigger reset before start()

    constructor(scene: SceneManager){

        // run parent's constructor()
        super(scene, {
            progressBarId: 'customizerOutro',
        });

        this.updateText();

    }

   
    public start(){

        if(this.runTimes > 0) this.reset();

        this.runTimes += 1;
        this.toggleParentClass();
        this.makeVisible();
        this.startAnimationLoop();
        this.pretendToLoad();
        this.scrollToTopOnMobile();
    }

    private scrollToTopOnMobile(){
        if(window.innerWidth < 768){
            window.scrollTo(0, 0);
        }
    }

    public finish(){
		this.set(100);
		this.animate();
    }
    
    
    public onComplete(){

        console.log('outro complete');

        this.cancelAnimation();
        this.$progressBarElement.classList.add('hide-progress');
        this.toggleParentClass();
        
    }
    
    public reset(){
        
        this.currentProgress = 0;
        this.delayedProgress = 0;
        this.done = false;
        this.$progressBarElement.classList.remove('loading','hide-progress');
        this.$currentProgress.style.width = this.currentProgress+'0';

        this.updateText();
    }

    public updateText(){
        if(this.texts[this.currentProgress]) this.setText(this.texts[this.currentProgress]);
    }


    protected followProgress(){
        this.updateText();
    }

    private pretendToLoad(){

        let scope = this;
        let progress = 0;
        let totalTime = 2000; // ms
        let max = 90;
        let increase = 5;
        let delay = totalTime / max * increase;
        let loop;
        
        setTimeout(()=>{
            loop = setInterval(()=>{
                fill();
            }, delay)
        }, 400); // first time its longer

        function fill(){
            progress += increase;
            if(progress <= max){ 
                scope.set(progress); 
            }else{
                clearInterval(loop);
            }
        }
    }

    private toggleParentClass(){
        document.getElementsByTagName('body')[0].classList.toggle('customizer-outro');
    }

}