import { SceneManager } from "./../view/SceneManager";

export class ProgressBar{

	protected currentProgress = 0;
	protected delayedProgress = 0;
	protected $currentProgress: HTMLElement;
	protected $progressBarElement : HTMLElement;
	protected _scene: SceneManager;
	protected done = false;
	protected loop:any;
	protected showCustomizerOnLoad: boolean = false;
	protected progressBarId = 'customizerProgressBar';

	constructor(scene: SceneManager, config?){

		this._scene = scene;
		if(config && config.progressBarId) this.progressBarId = config.progressBarId;

		this.$progressBarElement = <HTMLElement> document.getElementById(this.progressBarId);
		this.$currentProgress = <HTMLElement> this.$progressBarElement.querySelectorAll('.currentProgress')[0];

	}

	public setText(text){
		this.$progressBarElement.querySelectorAll('.progress-text')[0].innerHTML = text;
	}

	public makeVisible(){
		this.$progressBarElement.classList.add('loading');
		console.log('progress bar visible', this.done, this.currentProgress);
	}

	public finished(){
		return this.done;
	}

	public startAnimationLoop(){
		this.loop = requestAnimationFrame(()=>{
			this.animate(true);
		});
	}

    public update = (loaded, total) =>{

		let percent = loaded / total;
		const min = 10; // minimum is 10% loaded, so that user sees progress instantly
		if(percent < min) percent = min;
		if(percent > this.delayedProgress){
			this.delayedProgress = percent;
		}	

	};

	public set(progress){
		if(progress > this.delayedProgress && progress > 0 && progress <= 100){
			this.delayedProgress = progress;
		}
	}

	public add(value){
		let newProgress = Math.min(100, this.delayedProgress + value); // make sure its not above 100
		this.set(newProgress);
	}
	
	protected followProgress(){
		// in case u wanna execute code on specific %
		// this is used in Distraction.ts
	}

	protected animate = (loop: boolean = false)=>{

		if( ! this.done){
			if(this.currentProgress < this.delayedProgress){
				this.followProgress();
				this.currentProgress += 1;
				this.currentProgress = Math.min(100, this.currentProgress); // cap at 100
				this.$currentProgress.style.width = this.currentProgress+'%';
				// console.log('progress', this.currentProgress);
			}else if(this.currentProgress >= 100){
				this.done = true;
				// console.log('progress bar - DONE');
				this.onComplete();
			}
			if(loop===true){
				this.loop = requestAnimationFrame(()=>{
					this.animate(true);
				});
			}
		}

		return this.done;
	}

	public finish(){
		this.delayedProgress = 100;
		this.animate();
	}

	public onComplete(){

		// console.log('- progress bar - onComplete', this.showCustomizerOnLoad);

		this.cancelAnimation();
		this.$progressBarElement.classList.add('hide-progress');
		if(this.onCompleteCallback) this.onCompleteCallback();
		if(this.showCustomizerOnLoad) this._scene.controlsVisibilityToggle(true);
	}

	protected cancelAnimation(){
		cancelAnimationFrame(this.loop);
	}

	// this should only be called if silent loader is ON
	public queueAutoShow(){
		// console.log('- queue customizer visibility change');
		this.showCustomizerOnLoad = true;
	}

	public onCompleteCallback(){
		// set this function from outside to trigger extra stuff on load complete
	}
}