/*
 * Cloth Simulation using a relaxed constraints solver
 */

// Suggested Readings

// Advanced Character Physics by Thomas Jakobsen Character
// http://freespace.virgin.net/hugo.elias/models/m_cloth.htm
// http://en.wikipedia.org/wiki/Cloth_modeling
// http://cg.alexandra.dk/tag/spring-mass-system/
// Real-time Cloth Animation http://www.darwin3d.com/gamedev/articles/col0599.pdf

import { Vector3, Geometry, Mesh } from 'three';

// IMPORTANT - this doesnt work atm, use physics in ProductGarmet

export class ClothSimulator{

	// TODO - physics in ProductGarmet works for now, later on fix this and use it

	public static MASS = 0.1;
	public static restDistance = 25;
	public static DAMPING = 0.03;
	public static DRAG = 1 - ClothSimulator.DAMPING;
	public static ballSize = 60; //40
	public static xSegs = 10;
	public static ySegs = 15;
	public static windForce = new Vector3( 0, 0, 0 );
	public ballPosition = new Vector3( 0, - 45, 0 );
	public cloth;
	public clothFunction;

	private pins = [ 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ];

	private GRAVITY = 981 * 1.4;
	private gravity = new Vector3( 0, - this.GRAVITY, 0 ).multiplyScalar( ClothSimulator.MASS );

	private TIMESTEP = 18 / 1000;
	private TIMESTEP_SQ = this.TIMESTEP * this.TIMESTEP;

	private wind = true;
	private windStrength = 0.5; // default 2
	
	private tmpForce = new Vector3();

	private lastTime;

	private clothGeometry;

	private diff = new Vector3();

	private sphere;
	private _product;

	constructor(){ // add product link?

		// this._product = product;
		
		// this.clothGeometry = clothGeometry;
		// this.sphere = sphere;

		this.clothFunction = this.plane( ClothSimulator.restDistance * ClothSimulator.xSegs, ClothSimulator.restDistance * ClothSimulator.ySegs );
		this.cloth = new Cloth(this);
	}

	private plane( width, height ) {

		return function ( u, v, target ) {

			var x = ( u - 0.5 ) * width;
			var y = ( v + 0.5 ) * height;
			var z = 0;

			target.set( x, y, z );

		};

	}

	private satisfyConstraints( p1, p2, distance ) {

		this.diff.subVectors( p2.position, p1.position );
		var currentDist = this.diff.length();
		if ( currentDist === 0 ) return; // prevents division by 0
		var correction = this.diff.multiplyScalar( 1 - distance / currentDist );
		var correctionHalf = correction.multiplyScalar( 0.5 );
		p1.position.add( correctionHalf );
		p2.position.sub( correctionHalf );

	}

	public simulate( time ) {

		if ( ! this.lastTime ) {
			this.lastTime = time;
			return;
		}

		let il, particles, particle, pt, constraints, constraint;

		// Aerodynamics forces

		if ( this.wind ) {

			var indx;
			var normal = new Vector3();
			var indices = this.clothGeometry.index;
			var normals = this.clothGeometry.attributes.normal;

			particles = this.cloth.particles;

			for ( let i = 0, il = indices.count; i < il; i += 3 ) {

				for ( let j = 0; j < 3; j ++ ) {

					indx = indices.getX( i + j );
					normal.fromBufferAttribute( normals, indx )
					this.tmpForce.copy( normal ).normalize().multiplyScalar( normal.dot( ClothSimulator.windForce ) );
					particles[ indx ].addForce( this.tmpForce );

				}

			}

		}

		for ( let i=0, particles = this.cloth.particles, il = particles.length; i < il; i ++ ) {

			particle = particles[ i ];
			particle.addForce( this.gravity );

			particle.integrate( this.TIMESTEP_SQ );

		}

		// Start Constraints

		constraints = this.cloth.constraints;
		il = constraints.length;

		for ( let i = 0; i < il; i ++ ) {

			constraint = constraints[ i ];
			this.satisfyConstraints( constraint[ 0 ], constraint[ 1 ], constraint[ 2 ] );

		}

		// Ball Constraints

		this.ballPosition.z = - Math.sin( Date.now() / 600 ) * 90; //+ 40;
		this.ballPosition.x = Math.cos( Date.now() / 400 ) * 70;

		if ( this.sphere.visible ) {

			for ( let i = 0, particles = this.cloth.particles, il = particles.length; i < il; i ++ ) {

				particle = particles[ i ];
				var pos = particle.position;
				this.diff.subVectors( pos, this.ballPosition );
				if ( this.diff.length() < ClothSimulator.ballSize ) {

					// collided
					this.diff.normalize().multiplyScalar( ClothSimulator.ballSize );
					pos.copy( this.ballPosition ).add( this.diff );

				}

			}

		}


		// Floor Constraints

		for ( let i = 0, particles = this.cloth.particles, il = particles.length; i < il; i ++ ) {

			particle = particles[ i ];
			pos = particle.position;
			if ( pos.y < - 250 ) {
				pos.y = - 250;
			}

		}

		// Pin Constraints

		for ( let i = 0, il = this.pins.length; i < il; i ++ ) {

			var xy = this.pins[ i ];
			var p = particles[ xy ];
			p.position.copy( p.original );
			p.previous.copy( p.original );

		}


	}

}

export class Particle{

	private a;
	private mass;
	private invMass;
	private position = new Vector3();
	private previous = new Vector3();
	private original = new Vector3();
	private tmp = new Vector3();
	private tmp2 = new Vector3();

	private _clothSimulator;	

	constructor(clothSimulator, x, y, z, mass){

		this._clothSimulator = clothSimulator;
		
		this.a = new Vector3( 0, 0, 0 ); // acceleration
		this.mass = mass;
		this.invMass = 1 / mass;
	
		// init

		this._clothSimulator.clothFunction( x, y, this.position ); // position
		this._clothSimulator.clothFunction( x, y, this.previous ); // previous
		this._clothSimulator.clothFunction( x, y, this.original );
	}

	public addForce( force ) {

		this.a.add(
			this.tmp2.copy( force ).multiplyScalar( this.invMass )
		);
	
	};

	public integrate( timesq ) {

		var newPos = this.tmp.subVectors( this.position, this.previous );
		newPos.multiplyScalar( ClothSimulator.DRAG ).add( this.position );
		newPos.add( this.a.multiplyScalar( timesq ) );
	
		this.tmp = this.previous;
		this.previous = this.position;
		this.position = newPos;
	
		this.a.set( 0, 0, 0 );
	
	};
}

export class Cloth{

	public w;
	public h;
	private particles = [];
	private constraints = [];
	private _clothSimulator;

	constructor(clothSimulator, w: number = 10, h: number = 10){

		this._clothSimulator = clothSimulator;

		this.w = w;
		this.h = h;

		let v, u;

		// Create particles
		for ( v = 0; v <= h; v ++ ) {

			for ( u = 0; u <= w; u ++ ) {

				this.particles.push(
					new Particle( this._clothSimulator, u / w, v / h, 0, ClothSimulator.MASS )
				);

			}

		}

		// Structural

		for ( v = 0; v < h; v ++ ) {

			for ( u = 0; u < w; u ++ ) {

				this.constraints.push( [
					this.particles[ this.index( u, v ) ],
					this.particles[ this.index( u, v + 1 ) ],
					this._clothSimulator.restDistance
				] );

				this.constraints.push( [
					this.particles[ this.index( u, v ) ],
					this.particles[ this.index( u + 1, v ) ],
					this._clothSimulator.restDistance
				] );

			}

		}

		for ( u = w, v = 0; v < h; v ++ ) {

			this.constraints.push( [
				this.particles[ this.index( u, v ) ],
				this.particles[ this.index( u, v + 1 ) ],
				this._clothSimulator.restDistance

			] );

		}

		for ( v = h, u = 0; u < w; u ++ ) {

			this.constraints.push( [
				this.particles[ this.index( u, v ) ],
				this.particles[ this.index( u + 1, v ) ],
				this._clothSimulator.restDistance
			] );

		}


		// While many systems use shear and bend springs,
		// the relaxed constraints model seems to be just fine
		// using structural springs.
		// Shear
		// var diagonalDist = Math.sqrt(restDistance * restDistance * 2);


		// for (v=0;v<h;v++) {
		// 	for (u=0;u<w;u++) {

		// 		constraints.push([
		// 			particles[index(u, v)],
		// 			particles[index(u+1, v+1)],
		// 			diagonalDist
		// 		]);

		// 		constraints.push([
		// 			particles[index(u+1, v)],
		// 			particles[index(u, v+1)],
		// 			diagonalDist
		// 		]);

		// 	}
		// }

	}


	private index( u, v ) {

		return u + v * ( this.w + 1 );

	}
	

}
