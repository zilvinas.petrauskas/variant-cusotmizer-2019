import { Product } from 'Product';
import { COLOR_NAMES, COLOR_FEEDER, ALL_COLORS, GENDER_MALE_TEXT, GENDER_FEMALE_TEXT, BLANKET_BIG_TEXT, BLANKET_SMALL_TEXT } from 'view/constants';
import { NearestColor } from 'view/NearestColor';

export class DynamicPropertiesInjector{

    private _product;
    private _activeColors: any;
    private parentElement: HTMLElement;
    private parentElementId = 'dynamicProductProperties';
    private _config: any;
    private _customText = {};
    private nearestColor : NearestColor;
    private nonCustomizedProduct = true;

    constructor(product : Product){

        this.nearestColor = new NearestColor(ALL_COLORS);
        this._product = product;
        this._config = this._product.getItemConfig();

        this.parentElement = this.getParentElement();

    }

    public setActiveColors(activeColors){
        // console.log('-- update active colors', activeColors);
        this._activeColors = activeColors;
    }

    public init(){

        this.createColorInputs();
        this.createTextInputs();
        this.createScreenshotInput();
        this.customProductInput();
        this.createVariantInput();
        this.createOriginalProductUrlInput();
        this.createNonCustomizedProductField();
        this.createProductHandleInput(); // cc001
        this.createOriginalHandleInput(); // m0001_cc001

        this.createGenderInput(); // optional
        this.createBlanketSizeInput(); // optional
        // create gender
        // or
        // create 'size' ?
    }

    // trigger on custom text change - update text value
    public updateTextInput(name, newText, customTexts){

        let textInput = this.nameOfCustomText(name, true);
        
        let prop1 = this.getFullInputNameOfProperty(textInput);
        document.querySelectorAll(`#${this.parentElementId} input[name='${prop1}']`)[0].setAttribute('value', newText);

        this.updateAllTextInput(customTexts);
    }

    private updateAllTextInput(customTexts){

        let array = [];
        if(Object.keys(customTexts).length > 0){
            for(let name in customTexts){
                let text = customTexts[name].trim();
                if(text.length > 0){
                    array.push(text);
                }
            }
        }

        let value = '';
        if(array.length > 0) value = array.join(', ');

        let inputAllTextsName = this.nameOfAllTexts();
        let prop1 = this.getFullInputNameOfProperty(inputAllTextsName);
        let searchFor = `#${this.parentElementId} input[name='${prop1}']`;
        let inputOfAllTexts = document.querySelectorAll(searchFor);
        if(inputOfAllTexts && inputOfAllTexts[0]){
            inputOfAllTexts[0].setAttribute('value', value);
        }else{
            console.log('Missing inputAllTextsName', searchFor, inputOfAllTexts);
        }

    }

    // trigger on custom text change - update text color value
    public updateTextColor(name, newColorCode){

        let colorCode = this.fixColorCode(newColorCode);

        let textInput2 = this.nameOfCustomTextColor(name, true);

        let prop2 = this.getFullInputNameOfProperty(textInput2);
        let colorName = COLOR_NAMES[colorCode];
        let colorFeeder = this.getFeederString(colorName);
        let colorString = `${colorCode}|${colorName}|${colorFeeder}`;

        document.querySelectorAll(`#${this.parentElementId} input[name='${prop2}']`)[0].setAttribute('value', colorString);
    }


   

    public createColorInputs(){

        this.createAllColorsInput();

        for(let modelPart in this._activeColors){

            let colorCode = this.fixColorCode(this._activeColors[modelPart]);

            let inputName = this.nameOfColor(modelPart, true);           

            let colorName = COLOR_NAMES[colorCode];
            if(typeof colorName === 'undefined' || colorName === null){
                 console.warn('Missing color name for: ', colorCode, COLOR_NAMES);
            }

            let colorFeeder = this.getFeederString(colorName);
            let colorString = `${colorCode}|${colorName}|${colorFeeder}`;

            let input1 = this.createInput(inputName, colorString);

            this.parentElement.appendChild(input1);
        }
  
    }

     // trigger on color change
     public updateColorInput(modelPart, newColorCode){

       

        let colorCode = this.fixColorCode(newColorCode);

        let inputName = this.nameOfColor(modelPart, true); // text value
        let inputNameListColors = this.nameOfAllColors();

        console.log('-3', modelPart, newColorCode, '|', inputName);

        let prop1 = this.getFullInputNameOfProperty(inputName);
        let prop3 = this.getFullInputNameOfProperty(inputNameListColors);
        let colorName = COLOR_NAMES[colorCode];

        let searchColorTextInput = `#${this.parentElementId} input[name='${prop1}']`;
        let inputColorText = document.querySelectorAll(searchColorTextInput);
        if(inputColorText && inputColorText[0]){
            let colorFeeder = this.getFeederString(colorName);
            let colorString = `${colorCode}|${colorName}|${colorFeeder}`;
            inputColorText[0].setAttribute('value', colorString);
        }else{
            console.log('Missing inputColorText', searchColorTextInput, inputColorText);
        }

        let searchAllColorsInput = `#${this.parentElementId} input[name='${prop3}']`;
        let inputAllColors = document.querySelectorAll(searchAllColorsInput);
        if(inputAllColors && inputAllColors[0]){
            let listTheColors = this.getColorList();
            inputAllColors[0].setAttribute('value', listTheColors);
        }else{
            console.log('Missing inputAllColors', searchAllColorsInput, inputAllColors);
        }
       
    }

    public createAllColorsInput(){
        let inputNameListColors = this.nameOfAllColors();
        let listOfColors = this.getColorList();
        let input3 = this.createInput(inputNameListColors, listOfColors);
        this.parentElement.appendChild(input3);
    }

    private fixColorCode(colorCode){

        if(typeof COLOR_NAMES[colorCode] === 'undefined'){
            let correctColorCode = this.nearestColor.find(colorCode);
            if(correctColorCode) colorCode = correctColorCode;
        }

        return String(colorCode).toLowerCase();
    }

    private getFeederString(colorName){
        const number = this.getColorFeederNumber(colorName);
        return `Feeder ${number}`;
    }

    private getColorFeederNumber(colorName){

        let number: number = 0;
        for(let i in COLOR_FEEDER){
            if(COLOR_FEEDER[i] === colorName){
                number = Number(i);
                break;
            }
        }

        if(number === 0) console.warn('- FEEDER NOT FOUND', number);
        
        return number;
    }

    public createOriginalProductUrlInput(){
        let url = window.location.href;
        let input = this.createInput(this.nameOfUrlInput(), url);
        this.parentElement.appendChild(input);
    }
   
    public updateOriginalProductUrlInput(){
        let value = window.location.href;
        let prop = this.getFullInputNameOfProperty(this.nameOfUrlInput());
        let searchFor = `#${this.parentElementId} input[name='${prop}']`;
        let input = document.querySelectorAll(searchFor);
        if(input && input[0]){
            input[0].setAttribute('value', value);
        }else{
            console.log('Missing original product input', searchFor, input);
        }
    }

    public createVariantInput(){

        let inputName = this.nameOfCustomVariant();
        let input = this.createInput(inputName, ''); 
        this.parentElement.appendChild(input);
    }

    public updateVariantInput(value){

        let inputName = this.nameOfCustomVariant();
        let prop = this.getFullInputNameOfProperty(inputName);
        let searchFor = `#${this.parentElementId} input[name='${prop}']`;
        let input = document.querySelectorAll(searchFor);
        if(input && input[0]){
            input[0].setAttribute('value', value);
        }else{
            console.log('Missing variant input', searchFor, input);
        }

    }

    public createTextInputs(){

       
        if(this._config.customText){

            // merge all exts into one line here
            let allTextsName = this.nameOfAllTexts();
            let allTextsInput = this.createInput(allTextsName, '');
            this.parentElement.appendChild(allTextsInput);
            let allTexts = [];

            // hidden input for each text separately
            for(let i in this._config.customText){
                let customText = this._config.customText[i];
                // console.log('creating custom text input?', customText);
                let nameOfCustomText = this.nameOfCustomText(customText.name, true);
                let nameOfCustomTextColor = this.nameOfCustomTextColor(customText.name, true);

                if(String(nameOfCustomText).length === 0) console.warn('-- !! Add name attribute to customText', i, this._product.getProductId(), customText);

                // need custom text (string)
                // custom text color
                // custom color position

                let colorCode = this.fixColorCode(customText.color);
                let colorName = COLOR_NAMES[colorCode];
                let colorFeeder = this.getFeederString(colorName);
                let colorString = `${colorCode}|${colorName}|${colorFeeder}`;

                let defaultTextValue = '';
                if(customText.hasOwnProperty('value') && customText['value'].length > 0){
                    defaultTextValue = customText['value'];
                    allTexts[customText.name] = customText['value'];
                }
                
                let textInput1 = this.createInput(nameOfCustomText, defaultTextValue); // custom text input
                let textInput2 = this.createInput(nameOfCustomTextColor, colorString);
                this.parentElement.appendChild(textInput1);
                this.parentElement.appendChild(textInput2);
            }

            this.updateAllTextInput(allTexts);

        }else{
            console.log('product doesnt have custom text', this._config.customText);
        }
        
    }

    private createNonCustomizedProductField(){
        let inputName = this.nameOfNonCustomizedField();
        let input = this.createInput(inputName, 'yes'); // until something gets modified on customizer
        this.parentElement.appendChild(input);
    }

    // returns true if product is customized, false - if not
    public isCustomizedProduct(){
        return !this.nonCustomizedProduct;
    }

    public updateNonCustomiedProductField(){

        if( ! this.nonCustomizedProduct) return;

        this.nonCustomizedProduct = false;

        let inputName = this.nameOfNonCustomizedField();
        let prop = this.getFullInputNameOfProperty(inputName);
        let searchFor = `#${this.parentElementId} input[name='${prop}']`;
        let input = document.querySelectorAll(searchFor);
        if(input && input[0]){
            input[0].setAttribute('value', 'no'); // if something is changed on product - it wont be a default product anymore
        }else{
            console.log('Missing default product field', searchFor, input);
        }
    }


    private createProductHandleInput(){
        let inputName = '_Handle';
        let input = this.createInput(inputName, this._product.getProductId());
        this.parentElement.appendChild(input);
    }
    
    private createOriginalHandleInput(){
        let inputName = '_originalHandle';
        let input = this.createInput(inputName, this._product.getOriginalHandle());
        this.parentElement.appendChild(input);
    }

    private createGenderInput(){
        
        if(this._product.isSweater() ){

            let genderValue = GENDER_MALE_TEXT;
            if(this._product.isFemaleSweater()) genderValue = GENDER_FEMALE_TEXT;

            let inputName = 'Gender';
            let input = this.createInput(inputName, genderValue);
            this.parentElement.appendChild(input);

        }

    }

    private createBlanketSizeInput(){

        if(this._product.isBlanket() ){

            let blanketSize = BLANKET_BIG_TEXT;
            if(this._product.isSmallBlanket()) blanketSize = BLANKET_SMALL_TEXT;

            let inputName = 'Size';
            let input = this.createInput(inputName, blanketSize);
            this.parentElement.appendChild(input);

        }

    }

    public createScreenshotInput(){

        let name = this.nameOfScreenshot();
        let input = this.createInput(name, '');
        this.parentElement.appendChild(input);

    }

    public customProductInput(){
        let name = this.nameOfCustomProduct();
        let input = this.createInput(name, '');
        this.parentElement.appendChild(input);
    }

    public updateScreenshotInput(screenshot, width, height){

        let name = this.nameOfScreenshot();
        let prop = this.getFullInputNameOfProperty(name);
        let value = `${screenshot}|${width}|${height}`;
        let searchFor = `#${this.parentElementId} input[name='${prop}']`;
        let inputScreenshot = document.querySelectorAll(searchFor);
        if(inputScreenshot && inputScreenshot[0]){
            inputScreenshot[0].setAttribute('value', value);
        }else{
            console.log('Missing inputAllColors', searchFor, inputScreenshot);
        }
    }

    public updateCustomProductNumber(value){
        let name = this.nameOfCustomProduct();
        let prop = this.getFullInputNameOfProperty(name);
        let searchFor = `#${this.parentElementId} input[name='${prop}']`;
        let input = document.querySelectorAll(searchFor);
        if(input && input[0]){
            input[0].setAttribute('value', value);
        }else{
            console.log('Missing inputAllColors', searchFor, input);
        }
    }

    private nameOfUrlInput(){
        return '_originalUrl';
    }

    private nameOfCustomVariant(){
        return '_customVariant'; // if you ever change this, make sure to change it in shopify's "js_main.js"
    }

    private nameOfCustomProduct(){
        return '_customProduct';
    }

    private nameOfScreenshot(){
        return '_screenshot';
    }

    private nameOfNonCustomizedField(){
        return '_Non customized product';
    }

    private nameOfColor(modelPart: string, isHidden: boolean){
        let name = modelPart; //+' Color';
        if(isHidden) return `_${name}`;
        return name;
    }

    private nameOfColorCode(modelPart: string, isHidden: boolean){
        let name = modelPart+' Color Code';
        if(isHidden) return `_${name}`;
        return name;
    }

    private nameOfAllTexts(isHidden: boolean = false){
        let name = 'Text';
        if(isHidden) return `_${name}`;
        return name;
    }

    // input name for custom text, like "Text on Front";
    private nameOfCustomText(name: string, isHidden: boolean = false){
        if(isHidden) return `_${name}`;
        return name;
    }

    private nameOfCustomTextColor(modelPart: string, isHidden: boolean){
        let name = `${modelPart} Color`;
        if(isHidden) return `_${name}`;
        return name;
    }

    private nameOfAllColors(){
        return 'Colors';
    }

    private getColorList(unique: boolean = true){
        // return only unique colors as text values, these do not go to backend
        let arr = [];
        let uniqueList = {};
        for(let i in this._activeColors){

            let colorCode = this.fixColorCode(this._activeColors[i]);
            let colorName = COLOR_NAMES[colorCode];

            if(unique === true && uniqueList.hasOwnProperty(colorName)) continue;

            uniqueList[colorName] = true;
            arr.push( colorName );
        }
        return arr.join(", ");
    }


    private getParentElement(){
        return document.getElementById(this.parentElementId);
    }

    private createInput(name, value){
        var input = document.createElement("input");
        input.setAttribute('type','hidden');
        input.setAttribute('name', this.getFullInputNameOfProperty(name));
        input.setAttribute('value', value);
        return input;
    }


    private getFullInputNameOfProperty(name){
        return `properties[${name}]`;
    }

}