import { PNG2BMPConverter } from './PNG2BMPConverter';
import { AJAX } from 'utils/AJAX';
import { Main } from './../Main';
import { EXTERNAL_SERVER_URL, COLOR_NAMES, ALL_COLORS, GENDER_FEMALE_TEXT, GENDER_MALE_TEXT, BLANKET_SMALL_TEXT, BLANKET_BIG_TEXT, API_CONFIG } from './../view/constants';

export class OrderManager
{

	private _PNG2BMPConverter: PNG2BMPConverter;
	private _main: Main;
	private busy = false;

	private progressValues = {
		customizer: 70,
		variant: 30,
		image: 0, // we dont wait for this one to complete
	}

	constructor(PNG2BMPConverter: PNG2BMPConverter, main: Main)
	{
		this._PNG2BMPConverter = PNG2BMPConverter;
		this._main = main;				

		// EVENT ADD TO CART
		window.addEventListener('cart:add', ()=>{
			console.log('event - cart:add');
			this.saveCustomizedProduct();
		});

		window.addEventListener('cart:remove', ()=>{
			console.log('event - cart:remove');
		});


		// test
		window.addEventListener('outro:start', ()=>{
			console.log('event - outro:start');
			main._scene._productLoader.outro.start();

			setTimeout(()=>{
				main._scene._productLoader.outro.finish();
			},3000);
		});
		
		
		const testURL = this.getExternalServerUrl('customizer.php');
	}


	private outroStart(){ 	// start outro loading..
		this._main._scene._productLoader.outro.start();
	}

	private outroProgress(value){
		this._main._scene._productLoader.outro.add(value);
	}

	private hideCustomizer(){ // hide customizer
		
		(document.querySelectorAll('.customizerCancel')[0] as HTMLElement).click();
	}

	// for now its only text on some sweaters, so make sure input is not empty if config requires it
	private checkRequiredFields(){

		let valid = true;
		let error = '';
		
		let MISSING_TEXT_ERROR = 'Please enter required text';

		// check if text required and if input not empty
		const config = this._main.product.getItemConfig();
		if(config && config.customText){
			const textValues = this._PNG2BMPConverter.exportCustomTextData();
			console.log('- textValues', textValues);
			for(let i=0;i<config.customText.length;i++){
				const partConfig = config.customText[i];
				if(partConfig && partConfig.required === true){
					// check if input is not empty

					if(typeof textValues[partConfig.name] === 'undefined'){
						valid = false;
						error = MISSING_TEXT_ERROR;
						break;
					}

					let partText = textValues[partConfig.name].trim();
					if(partText.length === 0){
						valid = false;
						error = MISSING_TEXT_ERROR;
						break;
					}
				}
			}
		}

		if(!valid){
			alert(error);
		}

		return valid;
	}

	public saveCustomizedProduct(){

		if(this.busy) return; 
		if( ! this.checkRequiredFields() ) return;

		this.busy = true;
		this.toggleAddToCartButtons(true);
		this.outroStart();
		this.hideCustomizer();

		let scope = this;
		setTimeout(()=>{
			scope.gatherProductInfo();
		} ,550); // wait for outro transition and proceed, this should be bigger than transition speed of "body:not(.customizer-loaded) .playGroundFixed" in customizer.css


	}

	private toggleAddToCartButtons(disabled: boolean = false){
		let buttons = document.querySelectorAll('.customizer-add-cart');
		if(buttons && buttons.length > 0){
			for(let i=0;i<buttons.length;i++){
				if(disabled === true){
					buttons[i].setAttribute('disabled','disabled');
					buttons[i].classList.add('busy');
				}else{
					buttons[i].removeAttribute('disabled');
					buttons[i].classList.remove('busy');
				}
				
			}
		}
	}

	// we still need this functionality and save information about product
	// addToCart renamed and modified here
	public gatherProductInfo(){


		try{

			const handle = this._main.product.getProductId(); // this is actualla 'handle' not an product id in shopify admin
			const realID = document.getElementById('section-product').dataset.id;

			const isCustomized = this._main.product.dynamicProperties.isCustomizedProduct();

			// send these to backend
			let params: any = {
				post : 'save-product',
				product: {
					handle: handle, // CC001 e.g.
					originalHandle: this._main.product.getOriginalHandle(), // e.g. M0001_CC001
					id : realID, // id used in shopify
					variant: this._main.product.variant(),
					quantity: 1,
					colors: {},
				},
				// masks: this._PNG2BMPConverter.getMasks(),
				// screenshot: this._main.product.perfectScreenshot(), // returns object: image, width, height
				isCustomized: isCustomized,
			};

			// if(!isCustomized){ // enable when backend has default products
				params.masks = this._PNG2BMPConverter.getMasks();
				params.screenshot = this._main.product.perfectScreenshot(); // returns object: image, width, height
			// }

			const customText = this._PNG2BMPConverter.exportCustomTextData();
			if(customText && Object.keys(customText).length > 0){
				params.product.customText = {};
				for(let name in customText){
					if(customText[name].length > 0){
						params.product.customText[name] = customText[name];
					}
				}
			}
			

			// save custom colors
			const customColors = this._PNG2BMPConverter.exportCustomColors();
			if(customColors && Object.keys(customColors).length > 0){
				for(let name in customColors){
					let colorName = COLOR_NAMES[customColors[name]];
					params.product.colors[name] = `${customColors[name]} (${colorName})`;
				}
			}
			

			if(this._main.product.isBlanket()){

				
				params.product.type = 'blanket';

				if(this._main.product.isBigBlanket()){
					params.product.size = BLANKET_BIG_TEXT;
				}else{
					params.product.size = BLANKET_SMALL_TEXT;
				}

			}else{

				params.product.type = 'sweater';

				// get product size
				params.product.size = this._main.product.getSize();

				// save colors of collar, cuff & rib
				params.product.colors.collar = this._PNG2BMPConverter.getColorOfModelPartAndAddItsName('CLR');
				params.product.colors.cuff = this._PNG2BMPConverter.getColorOfModelPartAndAddItsName('CUF');
				params.product.colors.rib = this._PNG2BMPConverter.getColorOfModelPartAndAddItsName('RIB');

				params.variantText = this._main.product.getSize();

				if(this._main.product.isFemaleSweater()){
					params.product.gender = GENDER_FEMALE_TEXT;
				}else{
					params.product.gender = GENDER_MALE_TEXT;
				}

			}

			params.makeTemp = true;

			// console.log('saving order', params);

			this.saveProductInfo(params);

		}catch(err){
			console.log('ERROR!', err);
			this.toggleAddToCartButtons(false);
			this.busy = false;
		}
	}

	private getExternalServerUrl(file?){

		let url = '';

		if (this.onLocalhost()){

			// trigger local script on localhost
			url = this._main.getBaseUrl() + './api/' + file;

		}else{
			
			for(let i=0;i<API_CONFIG.length;i++){
				if(API_CONFIG[i].hostname == window.location.hostname){
					url = API_CONFIG[i].api + file;
					break;
				}
			}


		}

		return url;
	}

	private onLocalhost(){
		return window.location.hostname === "localhost" || window.location.hostname === "127.0.0.1";
	}

	private saveProductInfo(params){

		// probably you want to trigger your actual shopify add to cart fn too??
		let url = this.getExternalServerUrl('customizer.php');
		let isLocalhost = this.onLocalhost();
		

		AJAX.load({
			url: url,
			params: params,
			method: 'POST',
			xhrFields: {
				withCredentials: true
			},
			onComplete: (jsonResponse) =>
			{
				const resp = JSON.parse(jsonResponse);
				console.log('order saved?', resp);
			
				// each product needs its own hidden attributes

				if(! isLocalhost ){
				
					this.outroProgress(this.progressValues.customizer);

					const customItemImageKey = `customImage_${params.product.id}_${params.product.variant}`; // cant do this, image doesnt exist yet in cart (cart in shopify didnt add things yet)
					
					// replace original product image - use screenshot_public
					window.localStorage.setItem(customItemImageKey, resp.screenshot_public);
					waitToReplaceImage(customItemImageKey, resp.screenshot_public);

					// save screenshot information into custom properties (use screenshot)
					this.updateScreenshotProperty(resp.screenshot_public, params.screenshot.width, params.screenshot.height);

					// save folder with access to all external data
					this.updateInputForProductExternalInfo(resp.customProductNumber);

					// trigger shopify add to cart
					const btnCart = document.querySelectorAll('.cart-functions button[type="submit"]')[0] as HTMLElement;

					
					// update form values to match new product
					if(resp.temp){

						this.updateOriginalUrlProperty();
						this.updateVariantProperty(resp.temp.variant_id); // update hidden input

						this.updateTempProduct(resp.temp); // update screenshot

						setTimeout(()=>{
							btnCart.click();
						},250); // enough for input values to change..

						// const updated = this.updateNewProductValues(resp.temp).catch((e)=>{ alert(e)});
						// updated.then(()=>{
						// 	console.log('TRIGGER #2');
						// 	setTimeout(()=>{
						// 		alert('Cart submitted');
						// 		btnCart.click();
						// 	}, 250);
						// })

					}else{
						btnCart.click();
					}

					setTimeout(()=>{
						this.toggleAddToCartButtons(false);
					}, 1000);

				}else{

					const localDelay = 1500;

					// just for testing on localhost
					setTimeout(()=>{
						this._main._scene._productLoader.outro.finish();
					},localDelay);

					setTimeout(()=>{
						this.toggleAddToCartButtons(false);
					}, localDelay+700);
				}

				
				this.busy = false;

			}
		});



		// <input id="customizercustomimage" type="hidden" name="attributes[customizerCustomImage]" value="{{ cart.attributes["customizerCustomImage"] }}">

		let createHiddenInput = (_name, _array:boolean=false) => {

			const _input = document.createElement('input');
			_input.setAttribute('type', 'hidden');

			if(_array){
				_input.setAttribute('name', `attributes[${_name}][]`);
			}else{
				_input.setAttribute('name', `attributes[${_name}]`);
			}
			

			return _input;
		}

		let createCartProductColors = (colors, customColorsParent) => {

			if(!customColorsParent){
				// customColorsParent = document.createElement('div');
				// customColorsParent.classList.add('cart-product-colors');
				console.log('missing custom colors parent');
				return;
			}

			// this below should match things in cart_form
			if(Object.keys(colors).length > 0){
				for(let i in colors){
					if(  !colors.hasOwnProperty(i)) continue;
					let colorDiv = document.createElement('div');
					colorDiv.classList.add('custom-product-color-preview');
					colorDiv.style.backgroundColor = "#"+colors[i];
					customColorsParent.appendChild(colorDiv);
				}
			}

		}

		let waitToReplaceImage = (_id, _src)=>{

			let maxWait = 5000;
			let currentWait = 0;
			
			function wait(){
				let elem = document.getElementById(_id);
				if(elem){
					elem.setAttribute('src',_src);
				}else{
					let t = 50;
					currentWait += t;
					if(currentWait < maxWait){
						setTimeout(()=>{
							wait();
						}, t);
					}else{
						console.log('FAILED to replace img',_id);
					}
				}
			}

			wait();

		}

	}

	private updateScreenshotProperty(src, w, h){
		this._main.product.dynamicProperties.updateScreenshotInput(src, w, h);
	}
	private updateVariantProperty(value){
		this._main.product.dynamicProperties.updateVariantInput(value);
	}
	private updateOriginalUrlProperty(){
		this._main.product.dynamicProperties.updateOriginalProductUrlInput();
	}

	private updateInputForProductExternalInfo(customProductNumber){
		this._main.product.dynamicProperties.updateCustomProductNumber(customProductNumber);
	}

	private async updateNewProductValues(newProduct){

		let progress = {
			variant: null,
			product: null,
		}

		let scope = this;

		return new Promise<any>(async (resolve, reject) =>{

			try{

				if(newProduct.variant_id !== '') progress.variant = await scope._main.product.changeVariantValueInForm(newProduct.variant_id);
				// progress.product = await scope._main.product.changeFormId(newProduct.product_id);
				// console.log('~ change success?', progress);
				resolve(true);
					
			}catch(e){
				reject(e);
			}
		});

	}

	private updateTempProduct(params){

		let url = this.getExternalServerUrl('shopify.php');
		let isLocalhost = this.onLocalhost();

		if(isLocalhost){
			console.log('not updating product image on localhost');
			return;
		}

		params.post = 'update-image';

		// TODO - queue to shopify to update product image (async request just to make things work faster)
		AJAX.load({
			url: url,
			params: params,
			method: 'POST',
			xhrFields: {
				withCredentials: true
			},
			onComplete: (jsonResponse) =>{

				this.outroProgress(this.progressValues.image);
				const resp = JSON.parse(jsonResponse);
				console.log('Image finished updating', resp);				
			}
		});

	}


	

}