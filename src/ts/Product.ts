import { Main } from 'Main';
import { ITEMS_CONFIG, LOCALHOST_FALLBACK, BLANKET_BIG, BLANKET_SMALL, SWEATER_MALE, SWEATER_FEMALE } from './view/constants';
import { DynamicPropertiesInjector } from 'utils/DynamicPropertiesInjector';
import { Vector3 } from 'three';

declare const PRODUCT_ID: string; // this catches globally and then can be retrieved
declare const PRODUCT_HANDLE : string;

export class Product{

    private id: string;
    private _main: Main;
    private itemConfig : any;
    private originalHandle = '';

    public dynamicProperties: DynamicPropertiesInjector;

    constructor(main?: Main){

        this._main = main;

        let id;

        if (this._main.isLocalhost()) {

            // check if url has 'id' paramter e.g. ?id=cc001
            // if it does, use that product id ONLY ON LOCALHOST
            var quickID = this.getProductIdFromUrl();

            if(typeof quickID !== 'undefined' && quickID !== null){
                id = this.getProductIdFromHandle(quickID);
            }else{ // otherwise jsut use a product id supplied in constants
                id = this.getProductIdFromHandle(LOCALHOST_FALLBACK.PRODUCT_ID);
            }

        } else {
            // typeof PRODUCT_HANDLE !== 'undefined' && PRODUCT_HANDLE !== null ?
            id = this.getProductIdFromHandle(PRODUCT_HANDLE);
        }

        this.originalHandle = typeof PRODUCT_HANDLE !== 'undefined' && PRODUCT_HANDLE !== null ? PRODUCT_HANDLE : LOCALHOST_FALLBACK.PRODUCT_ID; // for packing slip to show M0001_CC001 or W0001_CCF001

        // const id = typeof PRODUCT_HANDLE !== 'undefined' && PRODUCT_HANDLE !== null ? this.getProductIdFromHandle(PRODUCT_HANDLE) : this.getProductIdFromHandle(LOCALHOST_FALLBACK.PRODUCT_ID); // for localhost testing need a fallback id when shopify doesnt provide one
        this.id = String(id).toLowerCase();
        
        console.log('PRODUCT ID?', this.id);

        this.dynamicProperties = new DynamicPropertiesInjector(this);

        this.itemConfig = this.getItemConfig();
    }

    private getProductIdFromUrl(){
        var url = new URL(window.location.href);
        var quickID = url.searchParams.get("id");
        return quickID;
    }

    public productModified() {
        this.dynamicProperties.updateNonCustomiedProductField();
    }

    private getProductIdFromHandle(handle: string) {

        if(typeof handle === 'undefined' || handle === null){
            console.warn('PRODUCT HANDLE NOT PROVIDED!', handle);
        }

        handle = handle.toLowerCase();
        let id;
        let replaceAll = ['m0001_','m0002_','m0003_','w0001_','w0002_','w0003_'];
        let spacer = "-";

        for(let i=0;i<replaceAll.length;i++){
            let replace = replaceAll[i];
            if(handle.search(replace) > -1){
                handle = handle.replace(replace,'');
            }
        }

        id = handle;

        // support SEO title here
        if(handle.search(spacer) > -1){
            let arr = handle.split(spacer);
            id = arr.pop();
        }


        return id;
    }

    public getOriginalHandle(){
        return this.originalHandle;
    }

    public getProductId(){
        return this.id; // this is handle
    }

    public getSize(){
        const options = document.querySelectorAll('ul.color-false[data-option="option1"] > li.active');
        console.log('size options?', options);
        return options[0].getAttribute('data-text'); // data-text -> XS, S, M, L, XL
    }

    public getItemConfig(){

        if(typeof this.itemConfig !== 'undefined' && this.itemConfig !== null){
            return this.itemConfig;
        }

        const code = this.getProductId().toUpperCase();

		if(ITEMS_CONFIG.hasOwnProperty(code)){
			return ITEMS_CONFIG[code];
		}
		return null;
    }
    
    private idHas(string){
        return this.id.search(string) > -1;
    }

    public isBlanket(){
        return this.idHas(BLANKET_BIG) || this.idHas(BLANKET_SMALL);
    }

    public isSweater(){
        return this.idHas(SWEATER_MALE) || this.idHas(SWEATER_FEMALE);
    }

    public isMaleSweater(){
        return this.idHas(SWEATER_MALE);
    }

    public isFemaleSweater(){
        return this.idHas(SWEATER_FEMALE);
    }

    public isBigBlanket(){
        return this.idHas(BLANKET_BIG);
    }

    public isSmallBlanket(){
        return this.idHas(BLANKET_SMALL);
    }

    public variant(){
        var url = new URL(window.location.href);
        var variant = url.searchParams.get("variant") || '';
        return variant;
    }

    

    // this is just a texture name with extension from configuration
    public getItemTexture(type){

        const texture = this.itemConfig.textures.masks[type];
        if (typeof texture === 'undefined' || texture === null) {
            console.warn('Missing texture from config for: ' + type, this.itemConfig.textures.masks);
        }
        return texture;
    }

    // this is just a normal texture name with extension
    public getItemNormalTexture(type){

        const normal = this.itemConfig.textures.normals[type];
        if(typeof normal === 'undefined' || normal === null){
            console.warn('Missing normal from config for: '+type, this.itemConfig.textures.normals);
        }
        return normal;
    }

    public async changeVariantValueInForm(newValue){

        return new Promise<any>((resolve, reject) =>{

            let element = document.querySelectorAll('#productSelect option[selected="selected"]');
            if(element && element[0]){
                element[0].setAttribute('value', newValue);
                // alert('Variant id changed: '+ element[0].getAttribute('value'));
                resolve(true);
            }else{
                alert('Variant element not found');
            }
            reject(false);
        });

      
    }

    public async changeFormId(newId){

        return new Promise<any>((resolve, reject) =>{

            let element = document.querySelectorAll('.shopify-product-form');
            if(element && element[0]){
                element[0].setAttribute('id', `product_form_${newId}`);
                resolve(true);
            }
            reject(false);

        });

       
    }

    public perfectScreenshot(){

        const desired = {
            width: 400,
            height: 400,
        }
        const pixelRatio = window.devicePixelRatio || 1;
        const width = desired.width / pixelRatio;
        const height = desired.height / pixelRatio;

        console.log('pixelRatio',pixelRatio);
       
        // save current camera position
        const oldPosition = this._main._scene.camera.position.clone();

        // change camera position for 1 frame
        if(this.isSweater()){
            this._main._scene.camera.position.set(0, 8, 662);
        }else{
            this._main._scene.camera.position.set(0, 50, 1050); // 1, 110, 1250
        }       

        this._main._scene.camera.lookAt(0,0,0);

        // set camera and renderer to desired screenshot dimension
        this._main._scene.camera.aspect = width / height;
        this._main._scene.camera.updateProjectionMatrix();
        this._main._scene.renderer.setSize( width, height );
        this._main._scene._productLoader.animate(); // update controls
        this._main._scene.triggerRender();


         // save image data
        const imageData = this._main._scene.renderer.domElement.toDataURL( 'image/png' );
       
         // once screenshot is captured, restore camera position
         this._main._scene.camera.position.copy(oldPosition);
         this._main._scene.camera.lookAt(0,0,0);

          // reset to old dimensions by invoking the on window resize function
        this._main._scene.onWindowResize();

         return {
             image: imageData,
             width: width,
             height: height,
         }

    }

    public screenshot(){

        const resizeEnabled = true;
        const max = { // fit resize within these bounds
            width: 400,
            height: 400
        }	

        let screenshot = '';

        // save current camera position
        const oldPosition = this._main._scene.camera.position.clone();

         // change camera position for 1 frame
         if(this.isSweater()){
            this._main._scene.camera.position.set(0, 50, 550);
        }else{
            this._main._scene.camera.position.set(0, 110, 1250);
        }

        Main.instance._scene.triggerRender();
		const canvas:any = document.getElementById('myCanvas');
	        
        let resized:any = {};

        if(resizeEnabled){
            /* fit canvas into these bounds */
            	  		
            let canvasCopy = <HTMLCanvasElement> document.createElement("canvas")
            canvasCopy.setAttribute('style','opacity: 0');
            const copyContext = canvasCopy.getContext("2d")
           
            if(canvas.width / canvas.height > max.width / max.height) { 
                resized.width = max.width;
                resized.height = (canvas.height * (max.width / canvas.width)).toFixed(0);
            }else { 
                resized.width = (canvas.width * (max.height / canvas.height)).toFixed(0);
                resized.height = max.height;
            }

            canvasCopy.width = resized.width
            canvasCopy.height = resized.height;
            copyContext.drawImage(canvas, 0, 0, canvas.width, canvas.height, 0, 0, canvasCopy.width, canvasCopy.height);

            screenshot = canvasCopy.toDataURL("image/png"); // WITH resize return resized canvas image
            /* end of resize */

        }else{
            screenshot = canvas.toDataURL("image/png"); // without resize return original canvas
        }
		

       
        
        // once screenshot is captured, restore camera position
        this._main._scene.camera.position.copy(oldPosition);

		return {
            image: screenshot,
            width: resized.width || max.width,
            height: resized.height || max.height,
        }

    }

    
}