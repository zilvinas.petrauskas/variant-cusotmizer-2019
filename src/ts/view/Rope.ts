import { CatmullRomCurve3, Vector3, MeshLambertMaterial, TubeBufferGeometry, Mesh, TextureLoader, MeshStandardMaterial } from 'three';
import { Main } from 'Main';

export class Rope{

    private _main: Main;
    private _textures: any;

    constructor(main: Main){
        this._main = main;
        const _baseUrl = this._main.getBaseUrl();
        this._textures = {
            rope : new TextureLoader().load(`${_baseUrl}rope1.png`),
        }
    }

    public forSweater(){

        var pipeSpline = new CatmullRomCurve3( [
            new Vector3( 0, 10, - 10 ), new Vector3( 10, 0, - 10 ),
            new Vector3( 20, 0, 0 ), new Vector3( 30, 0, 10 ),
            new Vector3( 30, 0, 20 ), new Vector3( 20, 0, 30 ),
            new Vector3( 10, 0, 30 ), new Vector3( 0, 0, 30 ),
            new Vector3( - 10, 10, 30 ), new Vector3( - 10, 20, 30 ),
            new Vector3( 0, 30, 30 ), new Vector3( 10, 30, 30 ),
            new Vector3( 20, 30, 15 ), new Vector3( 10, 30, 10 ),
            new Vector3( 0, 30, 10 ), new Vector3( - 10, 20, 10 ),
            new Vector3( - 10, 10, 10 ), new Vector3( 0, 0, 10 ),
            new Vector3( 10, - 10, 10 ), new Vector3( 20, - 15, 10 ),
            new Vector3( 30, - 15, 10 ), new Vector3( 40, - 15, 10 ),
            new Vector3( 50, - 15, 10 ), new Vector3( 60, 0, 10 ),
            new Vector3( 70, 0, 0 ), new Vector3( 80, 0, 0 ),
            new Vector3( 90, 0, 0 ), new Vector3( 100, 0, 0 )
        ] );

        const params = {
            scale: 1,
            extrusionSegments: 100,
            radiusSegments: 3,
            closed: false,
        };

                
        const extrudePath = pipeSpline;
        const geometry = new TubeBufferGeometry( extrudePath, params.extrusionSegments, 2, params.radiusSegments, params.closed );          
        const material = new MeshStandardMaterial( { 
            // color: 0xff00ff,
            
            map: this._textures.rope,
         } );
        const mesh = new Mesh( geometry, material );
        mesh.scale.set(params.scale, params.scale, params.scale);

        return mesh;
    }

    private ropeTextureMissing(err){
        console.log('Rope texture missing', err);
    }

}