
export const EXTERNAL_SERVER_URL = 'https://customizer.calamigosdrygoods.com/api/';
export const EXTERNAL_SERVER_URL2 = 'https://customizer2.calamigosdrygoods.com/api/';
export const EXTERNAL_ASSETS_URL = 'https://customizer.calamigosdrygoods.com/assets/';

export const API_CONFIG = [
    {
        hostname: 'calamigosdrygoods.com',
        api: EXTERNAL_SERVER_URL
    },
    {
        hostname: 'variant-malibu.myshopify.com',
        api: EXTERNAL_SERVER_URL
    },
    {
        hostname: 'calamigos-test.myshopify.com',
        api: EXTERNAL_SERVER_URL2
    }

];

export const PRODUCT_AUTO_ROTATION_ENABLED = false;

export const USE_EXTERNAL_ASSETS = true; // if textures should be loaded from external url
export const PANORAMA_ENABLED = false; // if 360 panorama enabled
export const TEXT_STITCH_ENABLED = true; // if text stitch is enabled

// gradient background colors
export const BACKGROUND_COLOR_START = 'ffffff';
export const BACKGROUND_COLOR_END = 'aeaeae';
// export const BACKGROUND_COLOR_END = '343434';

/* name of 3d models */
// export const SWEATER_3D_MODEL = 'shirt_v19.glb'; 
export const SWEATER_3D_MODEL = 'new_shirt_v16.glb';
// export const BLANKET_3D_MODEL = 'blanket_v2.glb'; // not used currently, creating blanket manually in code
export const JACQUARD_NORMAL = 'stitch_jacquard.png';

export const MODEL_FRONT = 'FRT';
export const MODEL_BACK = 'BCK';
export const MODEL_SLEEVE = 'SLV';
export const MODEL_COLLAR = 'CLR'; // apykakle
export const MODEL_RIB = 'RIB'; // juosta apacioje
export const MODEL_CUFF = 'CUF';  // rankogalis

export const COLOR_BLACK_SWAN = '090a07'; // black
export const COLOR_WHITE_COTTON = 'f5f3e3'; // whitish, brightyellowish
export const COLOR_MARINA_BLUE = '262847'; // darkish blue
export const COLOR_CLASSIC_CAMEL = '9f7d5e'; // brown
export const COLOR_ORANGE = 'df6536'; // orange
export const COLOR_FINE_FLANNEL = '737474'; // gray
export const COLOR_RED_HOT = '9f1c28'; // red
export const COLOR_PINE_FOREST = '173321'; // forest green

export const COLOR_NAMES = {};
COLOR_NAMES[COLOR_BLACK_SWAN] = 'Black';
COLOR_NAMES[COLOR_WHITE_COTTON] = 'White';
COLOR_NAMES[COLOR_MARINA_BLUE] = 'Blue';
COLOR_NAMES[COLOR_CLASSIC_CAMEL] = 'Camel';
COLOR_NAMES[COLOR_ORANGE] = 'Orange';
COLOR_NAMES[COLOR_FINE_FLANNEL] = 'Grey';
COLOR_NAMES[COLOR_RED_HOT] = 'Red';
COLOR_NAMES[COLOR_PINE_FOREST] = 'Green';

export const COLOR_FEEDER = {
    1: COLOR_NAMES[COLOR_BLACK_SWAN],
    2: COLOR_NAMES[COLOR_WHITE_COTTON],
    3: COLOR_NAMES[COLOR_MARINA_BLUE],
    4: COLOR_NAMES[COLOR_CLASSIC_CAMEL],
    5: COLOR_NAMES[COLOR_ORANGE],
    6: COLOR_NAMES[COLOR_FINE_FLANNEL],
    7: COLOR_NAMES[COLOR_RED_HOT],
    8: COLOR_NAMES[COLOR_PINE_FOREST],
}

export const AO_MAP_INTENSITY = 1.8;

export const ALL_COLORS = [
    COLOR_BLACK_SWAN,
    COLOR_WHITE_COTTON,
    COLOR_MARINA_BLUE,
    COLOR_CLASSIC_CAMEL,
    COLOR_ORANGE,
    COLOR_FINE_FLANNEL,
    COLOR_RED_HOT,
    COLOR_PINE_FOREST
];

const ALL_COLORS_BUT_ORANGE = [
    COLOR_BLACK_SWAN,
    COLOR_WHITE_COTTON,
    COLOR_MARINA_BLUE,
    COLOR_CLASSIC_CAMEL,
    COLOR_FINE_FLANNEL,
    COLOR_RED_HOT,
    COLOR_PINE_FOREST
];

export const SWEATER_CONFIG = {
    envMap: 'environment.jpg',
    aoMap: 'ao3.png', // base
    ribMap: 'ao3_rib_cuf.png', // rif + cuf
    collarMap: 'ao3_collar.png', // collar
}

export const GENDER_MALE_TEXT = 'Male';
export const GENDER_FEMALE_TEXT = 'Female';
export const BLANKET_BIG_TEXT = 'Big Blanket';
export const BLANKET_SMALL_TEXT = 'Small Blanket';

// how to distinquig different type of products 
// MAKE SURE THESE ARE ALL LOWERCASED
export const SWEATER_MALE = 'cc0';
export const SWEATER_FEMALE = 'ccf';
export const BLANKET_BIG = 'ccb0';
export const BLANKET_SMALL = 'ccbb';

// make sure key here is always uppercased
export const ITEMS_CONFIG = {

    'CC001': {
        names: {
            'Rib and Cuff': 'Rib & Cuff Color',
        },
        masks: {
            'Rib and Cuff': ['RIB0', 'CUF0'],
        },
        colors: {
            'Rib and Cuff': [
                // COLOR_BLACK_SWAN,
                // COLOR_WHITE_COTTON,
                COLOR_MARINA_BLUE,
                COLOR_CLASSIC_CAMEL,
                COLOR_ORANGE,
                COLOR_FINE_FLANNEL,
                COLOR_RED_HOT,
                COLOR_PINE_FOREST
            ],
        },
        predefined: {
            CLR: COLOR_WHITE_COTTON,
            CUF: COLOR_RED_HOT,
            RIB: COLOR_RED_HOT,
        },

        // texture images and normal textures
        textures : {
            masks : {
                FRT : 'CC001_F.png',
                BCK: 'CC001_B.png',
                SLV: 'CC001_SLV.png',                
            },
            normals: {
                FRT: 'CC001_F_normal.png',
                BCK: 'CC001_B_normal.png',
                SLV: 'CC001_SLV_normal.png',                
            }
        },
    },
    

    'CC002': {
        names: {
            'Body': 'Body Color',
        },
        masks: {
            'Body': ['F0', 'B0', 'SLV0', 'CLR0', 'RIB0', 'CUF0'],
        },
        colors: {
            'Body': [COLOR_WHITE_COTTON, COLOR_FINE_FLANNEL],
        },
        predefined: {
            CLR: COLOR_WHITE_COTTON,
            CUF: COLOR_WHITE_COTTON,
            RIB: COLOR_WHITE_COTTON,
        },
        // customText: [
        //     {
        //         name: 'Text on Sleeve',
        //         position: 'SLV',
        //         limit: 2,
        //         color: COLOR_PINE_FOREST,
        //     }
        // ],
        textures : {
            masks : {
                FRT : 'CC002_F.png',
                BCK: 'CC002_B.png',
                SLV: 'CC002_SLV.png',                
            },
            normals: {
                FRT: 'CC002_F_normal.png',
                BCK: 'CC002_B_normal.png',
                SLV: 'CC002_SLV_normal.png',                
            }
        },

    },

    'CC003': {
        names: {
            'Body': 'Body Color',
        },
        masks: {
            'Body': ['F0', 'B0', 'SLV0', 'CLR0', 'RIB0', 'CUF0'],
        },
        colors: {
            'Body': [COLOR_WHITE_COTTON, COLOR_FINE_FLANNEL],
        },
        predefined: {
            CLR: COLOR_FINE_FLANNEL,
            CUF: COLOR_FINE_FLANNEL,
            RIB: COLOR_FINE_FLANNEL,
        },
        textures : {
            masks : {
                FRT : 'CC003_F.png',
                BCK: 'CC003_B.png',
                SLV: 'CC003_SLV.png',                
            },
            normals: {
                FRT: 'CC003_F_normal.png',
                BCK: 'CC003_B_normal.png',
                SLV: 'CC003_SLV_normal.png',                
            }
        },
    },

    'CC004': {
        names: {
            'Body': 'Body Color',
            'Top Stripe': 'Top Stripe Color',
            'Bottom Stripe': 'Bottom Stripe Color',
        },
        masks: {
            'Body': ['F0', 'B0', 'SLV0', 'RIB0', 'CLR0', 'CUF0'],
            'Top Stripe': ['SLV1'],
            'Bottom Stripe': ['SLV2'],
        },
        colors: {
            'Body': [...ALL_COLORS],
            'Top Stripe': [...ALL_COLORS],
            'Bottom Stripe': [...ALL_COLORS],
        },
        predefined: {
            CLR: COLOR_PINE_FOREST,
            CUF: COLOR_PINE_FOREST,
            RIB: COLOR_PINE_FOREST,
        },
        textures : {
            masks : {
                FRT : 'CC004_F.png',
                BCK: 'CC004_B.png',
                SLV: 'CC004_SLV.png',                
            },
            normals: {
                FRT: 'CC004_F_normal.png',
                BCK: 'CC004_B_normal.png',
                SLV: 'CC004_SLV_normal.png',                
            }
        },
    },

    'CC005': {
        names: {
            'Body': 'Body Color',
            'Top Pinstripe': 'Top Pinstripe',
            'Bottom Pinstripe': 'Bottom Pinstripe',
            'Collar': 'Collar Color',
        },
        masks: {
            'Body': ['F0', 'B0', 'SLV0', 'RIB0', 'CUF0'],
            'Top Pinstripe': ['F2', 'B2', 'SLV1'],
            'Bottom Pinstripe': ['F1', 'B1', 'SLV2'],
            'Collar': ['CLR0'],
        },
        colors: {
            'Body': [...ALL_COLORS],
            'Top Pinstripe': [...ALL_COLORS],
            'Bottom Pinstripe': [...ALL_COLORS],
            'Collar': [...ALL_COLORS],
        },
        customRules: {
            // 'Body': {
            //     differentFrom: ['Collar'],
            // }
            'Top Pinstripe': {
                differentFrom: ['Collar'],
            },
            'Bottom Pinstripe': {
                differentFrom: ['Collar'],
            },
             // 'Collar': {
            //     differentFrom: ['Body','Top Pinstripe','Bottom Pinstripe'],
            // },

        },
        predefined: {
            CLR: COLOR_CLASSIC_CAMEL,
            CUF: COLOR_CLASSIC_CAMEL,
            RIB: COLOR_CLASSIC_CAMEL,
        },
        textures : {
            masks : {
                FRT : 'CC005_F.png',
                BCK: 'CC005_B.png',
                SLV: 'CC005_SLV.png',                
            },
            normals: {
                FRT: 'CC005_F_normal.png',
                BCK: 'CC005_B_normal.png',
                SLV: 'CC005_SLV_normal.png',                
            }
        },
    },

    'CC006': {
        names: {
            'Body': 'Body Color',
            'Top Pinstripe': 'Top Pinstripe',
            'Bottom Pinstripe': 'Bottom Pinstripe',
            'Collar': 'Collar Color',
        },
        masks: {
            'Body': ['F0', 'B0', 'SLV0', 'RIB0', 'CUF0'],
            'Top Pinstripe': ['F1', 'B1', 'SLV2'],
            'Bottom Pinstripe': ['F2', 'B2', 'SLV1'],
            'Collar': ['CLR0'],
        },
        colors: {
            'Body': [...ALL_COLORS],
            'Top Pinstripe': [...ALL_COLORS],
            'Bottom Pinstripe': [...ALL_COLORS],
            'Collar': [...ALL_COLORS],
        },
        customRules: {
            // 'Body': {
            //     differentFrom: ['Collar'],
            // },
            'Top Pinstripe': {
                differentFrom: ['Collar'],
            },
            'Bottom Pinstripe': {
                differentFrom: ['Collar'],
            },
            // 'Collar': {
            //     differentFrom: ['Body', 'Top Pinstripe', 'Bottom Pinstripe'],
            // }
        },
        predefined: {
            CLR: COLOR_ORANGE,
            CUF: COLOR_PINE_FOREST,
            RIB: COLOR_PINE_FOREST,
        },
        textures : {
            masks : {
                FRT : 'CC006_F.png',
                BCK: 'CC006_B.png',
                SLV: 'CC006_SLV.png',                
            },
            normals: {
                FRT: 'CC006_F_normal.png',
                BCK: 'CC006_B_normal.png',
                SLV: 'CC006_SLV_normal.png',                
            }
        },
    },

    'CC007': {
        names: {
            'Collar': 'Collar and Text Color',
            'Rib and Cuff': 'Rib & Cuff Color',
            // 'Letters': 'Text Color',
        },
        masks: {
            'Collar': ['CLR0','F3', 'B3','FRT_TEXT'],
            'Rib and Cuff': ['RIB0', 'CUF0'],
            // 'Letters': ['F3', 'B3','FRT_TEXT'],
        },
        colors: {
            'Collar': [
                COLOR_BLACK_SWAN,
                // COLOR_WHITE_COTTON,
                // COLOR_MARINA_BLUE,
                COLOR_CLASSIC_CAMEL,
                COLOR_ORANGE,
                COLOR_FINE_FLANNEL,
                // COLOR_RED_HOT,
                COLOR_PINE_FOREST
            ],
            'Rib and Cuff': [
                COLOR_BLACK_SWAN,
                // COLOR_WHITE_COTTON,
                // COLOR_MARINA_BLUE,
                COLOR_CLASSIC_CAMEL,
                COLOR_ORANGE,
                COLOR_FINE_FLANNEL,
                // COLOR_RED_HOT,
                COLOR_PINE_FOREST
            ],
            // 'Letters': [...ALL_COLORS],
        },
        customRules: {
            'Collar': {
                differentFrom: ['Rib and Cuff'],
            },
            'Rib and Cuff': {
                differentFrom: ['Collar'],
            },
        },
        predefined: {
            CLR: COLOR_ORANGE,
            CUF: COLOR_PINE_FOREST,
            RIB: COLOR_PINE_FOREST,
        },
        customText: [
            {
                name: 'Text on Front',
                position: 'FRT',
                value: 'W', // default text
                required: true, // if text is required or optional
                color: COLOR_ORANGE,
                follow: 'CLR0', // follow color of model part, when input is changed
                stitch: 'JCQ',
            }
        ],
        textures : {
            masks : {
                FRT : 'CC007_F.png',
                BCK: 'CC007_B.png',
                SLV: 'CC007_SLV.png',                
            },
            normals: {
                FRT: 'CC007_F_normal.png',
                BCK: 'CC007_B_normal.png',
                SLV: 'CC007_SLV_normal.png',                
            }
        },
    },

    'CC008': {
        names: {
            'Collar': 'Collar Color', // and Text
            'Rib and Cuff': 'Rib & Cuff Color',
            // 'Letters': 'Text Color',
            // 'Stripe1' : 'Stripe #1', // for testing
            // 'Stripe2' : 'Stripe #2', // for testing
            // 'Stripe3' : 'Stripe #3', // for testing
            // 'Stripe4' : 'Stripe #4 (Stitch)', // for testing
            // 'Stripe4b' : 'Stripe #4 (Color)', // for testing
        },
        masks: {
            'Collar': ['CLR0', 'F4','B4', 'SLV3', 'FRT_TEXT'], // 'F4','B4','FRT_TEXT' - DONT CHANGE TEXT UNTIL BUG IS FIXED FOR LETTER CHANGING STRIPE COLOR TOO...
            // 'Collar': ['CLR0'], // 'F4','B4','FRT_TEXT' - DONT CHANGE TEXT UNTIL BUG IS FIXED FOR LETTER CHANGING STRIPE COLOR TOO...
            'Rib and Cuff': ['RIB0', 'CUF0'],
            // 'Letters': ['F4','B4','FRT_TEXT'],
            // 'Stripe1' : ['F0','B0','SLV0'], // grey
            // 'Stripe2' : ['F1','B1','SLV1'], // green
            // 'Stripe3' : ['F2','B2','SLV2'], // white
            // 'Stripe4' : ['F3','B3','SLV4'], // upper red is actually a brown stitch
            // 'Stripe4b' : ['F4','B4','SLV3'], // lower red is a color behind a stitch | BUG: when this color changes it affects letter color too for some reason
        },
        colors: {
            'Collar': [
                COLOR_BLACK_SWAN,
                // COLOR_WHITE_COTTON,
                COLOR_MARINA_BLUE,
                // COLOR_CLASSIC_CAMEL,
                COLOR_ORANGE,
                // COLOR_FINE_FLANNEL,
                COLOR_RED_HOT,
                // COLOR_PINE_FOREST
            ],
            'Rib and Cuff': [
                COLOR_BLACK_SWAN,
                // COLOR_WHITE_COTTON,
                COLOR_MARINA_BLUE,
                // COLOR_CLASSIC_CAMEL,
                COLOR_ORANGE,
                // COLOR_FINE_FLANNEL,
                COLOR_RED_HOT,
                // COLOR_PINE_FOREST
            ],
            // 'Letters': [...ALL_COLORS],
            // 'Stripe1': [...ALL_COLORS],
            // 'Stripe2': [...ALL_COLORS],
            // 'Stripe3': [...ALL_COLORS],
            // 'Stripe4': [...ALL_COLORS],
            // 'Stripe4b': [...ALL_COLORS],
        },
        customRules: {
            'Collar': {
                differentFrom: ['Rib and Cuff'],
            },
            'Rib and Cuff': {
                differentFrom: ['Collar'],
            },
        },
        predefined: {
            CLR: COLOR_RED_HOT,
            CUF: COLOR_MARINA_BLUE,
            RIB: COLOR_MARINA_BLUE,
        },
        customText: [
            {
                name: 'Text on Front',
                position: 'FRT',
                color: COLOR_RED_HOT,
                value: 'G', // default text
                required: true, // if text is required or optional
                follow: 'CLR0', // follow color of model part, when input is changed
                stitch: 'JCQ',
            }
        ],
        textures : {
            masks : {
                FRT : 'CC008_F.png',
                BCK: 'CC008_B.png',
                SLV: 'CC008_SLV.png',                
            },
            normals: {
                FRT: 'CC008_F_normal.png',
                BCK: 'CC008_B_normal.png',
                SLV: 'CC008_SLV_normal.png',                
            }
        },
    },

    'CC009': {

        names: {
            'Collar': 'Collar and Text Color',
            'Rib and Cuff': 'Rib & Cuff Color',
            // 'Letters': 'Text Color',
        },
        masks: {
            'Collar': ['CLR0','F5','B4','FRT_TEXT'],
            'Rib and Cuff': ['RIB0', 'CUF0'],
            // 'Letters': ['F5','B4','FRT_TEXT'],
        },
        colors: {
            'Collar': [
                // COLOR_BLACK_SWAN,
                // COLOR_WHITE_COTTON,
                COLOR_MARINA_BLUE,
                // COLOR_CLASSIC_CAMEL,
                COLOR_ORANGE,
                COLOR_FINE_FLANNEL,
                // COLOR_RED_HOT,
                COLOR_PINE_FOREST
            ],
            'Rib and Cuff': [
                // COLOR_BLACK_SWAN,
                // COLOR_WHITE_COTTON,
                COLOR_MARINA_BLUE,
                // COLOR_CLASSIC_CAMEL,
                COLOR_ORANGE,
                COLOR_FINE_FLANNEL,
                // COLOR_RED_HOT,
                COLOR_PINE_FOREST
            ],
            // 'Letters': [...ALL_COLORS],
        },
        customRules: {
            'Collar': {
                differentFrom: ['Rib and Cuff'],
            },
            'Rib and Cuff': {
                differentFrom: ['Collar'],
            },
        },
        predefined: {
            CLR: COLOR_MARINA_BLUE,
            CUF: COLOR_PINE_FOREST,
            RIB: COLOR_PINE_FOREST,
        },
        customText: [
            {
                name: 'Text on Front',
                position: 'FRT',
                color: COLOR_MARINA_BLUE,
                value: 'Z', // default text
                required: true, // if text is required or optional
                follow: 'CLR0', // follow color of model part, when input is changed
                stitch: 'JCQ',
            }
        ],
        textures : {
            masks : {
                FRT : 'CC009_F.png',
                BCK: 'CC009_B.png',
                SLV: 'CC009_SLV.png',                
            },
            normals: {
                FRT: 'CC009_F_normal.png',
                BCK: 'CC009_B_normal.png',
                SLV: 'CC009_SLV_normal.png',                
            }
        },
    },

    'CC010': {
        names: {
            'Body': 'Body Color',
            'Rope': 'Rope Color',
        },
        masks: {
            'Body': ['F0', 'F2', 'B0', 'SLV0', 'SLV3', 'CLR0', 'RIB0', 'CUF0','CLR0'], // F2 is actual front (keeping F0 in case texture is); SLV3 for oval part within rope on sleeves
            'Rope': ['F1', 'B1', 'SLV1', 'SLV2'], // slv1 - on body, slv2 - on sides
        },
        colors: {
            'Body': [...ALL_COLORS],
            'Rope': [...ALL_COLORS],
        },
        predefined: {
            CLR: COLOR_BLACK_SWAN,
            CUF: COLOR_BLACK_SWAN,
            RIB: COLOR_BLACK_SWAN,
        },
        customText: [
            {
                name: 'Text on Front',
                position: 'FRT',
                color: COLOR_WHITE_COTTON,
                stitch: 'JCQ',
                required: true,
                value: 'A',
                follow: 'FRT1', // rope color on front
            }
        ],
        textures : {
            masks : {
                FRT: 'CC010_F.png',
                BCK: 'CC010_B.png',
                SLV: 'CC010_SLV.png',                
            },
            normals: {
                FRT: 'CC010_F_normal.png',
                BCK: 'CC010_B_normal.png',
                SLV: 'CC010_SLV_normal.png',                
            }
        },
    },

    'CC011': {
        names: {
            'Body': 'Body Color',
            'Logo': 'Logo Color',
            // 'Collar': 'Collar Color',
        },
        masks: {
            'Body': ['F0', 'B0', 'SLV0', 'RIB0', 'CUF0', 'CLR0'],
            'Logo': ['F1'],
            // 'Collar': ['CLR0'],
        },
        colors: {
            'Body': [...ALL_COLORS],
            'Logo': [...ALL_COLORS],
            // 'Collar': [...ALL_COLORS],
        },
        customRules: {
            // 'Body': {
            //     differentFrom: ['Collar']
            // },
            // 'Logo': {
            //     differentFrom: ['Collar']
            // },
            // 'Collar': {
            //     differentFrom: ['Body', 'Logo']
            // },
        },
        predefined: {
            CLR: COLOR_MARINA_BLUE,
            CUF: COLOR_MARINA_BLUE,
            RIB: COLOR_MARINA_BLUE,
        },
        textures : {
            masks : {
                FRT : 'CC011_F.png',
                BCK: 'CC011_B.png',
                SLV: 'CC011_SLV.png',                
            },
            normals: {
                FRT: 'CC011_F_normal.png',
                BCK: 'CC011_B_normal.png',
                SLV: 'CC011_SLV_normal.png',                
            }
        },
    },

    'CC012': {
        names: {
            'Body': 'Body Color',
            'Logo': 'Logo Color',
        },
        masks: {
            'Body': ['F0', 'B0', 'SLV0', 'CLR0', 'RIB0', 'CUF0'], // F2 is actual front (keeping F0 in case texture is); SLV3 for oval part within rope on sleeves
            'Logo': ['F1'], // slv1 - on body, slv2 - on sides
        },
        colors: {
            'Body': [...ALL_COLORS],
            'Logo': [...ALL_COLORS],
        },
        predefined: {
            CLR: COLOR_WHITE_COTTON,
            CUF: COLOR_WHITE_COTTON,
            RIB: COLOR_WHITE_COTTON,
        },
        textures : {
            masks : {
                FRT: 'CC012_F.png',
                BCK: 'CC012_B.png',
                SLV: 'CC012_SLV.png',                
            },
            normals: {
                FRT: 'CC012_F_normal.png',
                BCK: 'CC012_B_normal.png',
                SLV: 'CC012_SLV_normal.png',                
            }
        },
    },

    'CC013': {
        names: {
            'Body': 'Body Color',
            'Top Pinstripe': 'Top Pinstripe',
            'Bottom Pinstripe': 'Bottom Pinstripe',
        },
        masks: {
            'Body': ['F0', 'B0', 'SLV0', 'RIB0', 'CUF0', 'CLR0'],
            'Top Pinstripe': ['F2', 'B2', 'F3'],
            'Bottom Pinstripe': ['F1', 'B1'],
        },
        colors: {
            'Body': [...ALL_COLORS],
            'Top Pinstripe': [...ALL_COLORS],
            'Bottom Pinstripe': [...ALL_COLORS],
        },
        predefined: {
            CLR: COLOR_FINE_FLANNEL,
            CUF: COLOR_FINE_FLANNEL,
            RIB: COLOR_FINE_FLANNEL,
        },
        textures : {
            masks : {
                FRT : 'CC013_F.png',
                BCK: 'CC013_B.png',
                SLV: 'CC013_SLV.png',                
            },
            normals: {
                FRT: 'CC013_F_normal.png',
                BCK: 'CC013_B_normal.png',
                SLV: 'CC013_SLV_normal.png',                
            }
        },
    },

    'CC014': {
        names: {
            'Body': 'Body Color',
            'Top Pinstripe': 'Top Pinstripe',
            'Bottom Pinstripe': 'Bottom Pinstripe',
            'Rib and Cuff': 'Rib & Cuff Color',
        },
        masks: {
            'Body': ['F0', 'B0', 'SLV0', 'CLR0'],
            'Top Pinstripe': ['F2', 'B2'],
            'Bottom Pinstripe': ['F1', 'B1'],
            'Rib and Cuff': ['RIB0', 'CUF0'],
        },
        colors: {
            'Body': [...ALL_COLORS],
            'Top Pinstripe': [...ALL_COLORS],
            'Bottom Pinstripe': [...ALL_COLORS],
            'Rib and Cuff': [...ALL_COLORS],
        },
        customRules: {
            'Body': {
                differentFrom: ['Rib and Cuff']
            },
            'Top Pinstripe': {
                differentFrom: ['Rib and Cuff']
            },
            'Bottom Pinstripe': {
                differentFrom: ['Rib and Cuff']
            },
            'Rib and Cuff': {
                differentFrom: ['Body', 'Top Pinstripe', 'Bottom Pinstripe']
            },
        },
        predefined: {
            CLR: COLOR_MARINA_BLUE,
            CUF: COLOR_BLACK_SWAN,
            RIB: COLOR_BLACK_SWAN,
        },
        textures : {
            masks : {
                FRT : 'CC014_F.png',
                BCK: 'CC014_B.png',
                SLV: 'CC014_SLV.png',                
            },
            normals: {
                FRT: 'CC014_F_normal.png',
                BCK: 'CC014_B_normal.png',
                SLV: 'CC014_SLV_normal.png',
            }
        },
    },

    // garmet
    'CCB001': {
        names: {
            'Body': 'Top Stripe',
            'Stripe': 'Bottom Stripe',
            'Top Arrow': 'Top Arrow',
            'Bottom Arrow': 'Bottom Arrow',
        },
        masks: {
            'Body': ['BLANKET0'],
            'Stripe': ['BLANKET2'],
            'Top Arrow': ['BLANKET1'],
            'Bottom Arrow': ['BLANKET3'],

        },
        colors: {
            'Body': [...ALL_COLORS_BUT_ORANGE],
            'Stripe': [...ALL_COLORS],
            'Top Arrow': [...ALL_COLORS],
            'Bottom Arrow': [...ALL_COLORS],
        },
        customText: [
            {
                name: 'Text on Top',
                position: 'BLANKET_TOP',
                y: 95,
                limit: 14,
                required: true,
                value: 'EST 1937',
                color: COLOR_PINE_FOREST,
                stitch: 'JCQ',
                follow: 'BLANKET1',
            },
            {
                name: 'Text on Bottom',
                position: 'BLANKET_BOT',
                y: 95,
                limit: 14,
                required: true,
                value: 'CALAMIGOS',
                color: COLOR_ORANGE,
                stitch: 'JCQ',
                follow: 'BLANKET3',
            }
        ],
        textures : {
            masks : {
                BLANKET: 'CCB001.png',
            },
            normals: {
                // BLANKET: 'CCB001_normal.png',
            }
        },
    },

    // garmet
    'CCB002': {
        names: {
            'Body': 'Body Color',
            'Top Stripe': 'Top Stripe',
            'Bottom Stripe': 'Bottom Stripe',
        },
        masks: {
            'Body': ['BLANKET1'],
            'Top Stripe': ['BLANKET0'],
            'Bottom Stripe': ['BLANKET2'],
        },
        colors: {
            'Body': [...ALL_COLORS_BUT_ORANGE],
            'Top Stripe': [...ALL_COLORS],
            'Bottom Stripe': [...ALL_COLORS],
        },
        customText: [
            {
                name: 'Text on Top',
                position: 'BLANKET_TOP',
                limit: 14,
                required: true,
                value: 'CA 90265',
                color: COLOR_MARINA_BLUE,
                stitch: 'JCQ',
                follow: 'BLANKET0', // above is default color, but if BLANKET0 is changed, text color will follow
            },
            {
                name: 'Text on Bottom',
                position: 'BLANKET_BOT',
                limit: 14,
                required: true,
                value: 'MALIBU',
                color: COLOR_MARINA_BLUE,
                stitch: 'JCQ',
                follow: 'BLANKET0',
            }
        ],

        textures : {
            masks : {
                BLANKET: 'CCB002.png',
            },
            normals: {
                // BLANKET: 'CCB002_normal.png',
            }
        },

    },

    'CCB003': {
        names: {
            'Edge': 'Edge Color',
            'Body': 'Body Color',
            'Letters': 'Text Color',
        },
        masks: {
            'Edge': ['BLANKET0'],
            'Body': ['BLANKET1'],
            'Letters': ['BLANKET2'],
        },
        colors: {
            'Edge': [...ALL_COLORS],
            'Body': [...ALL_COLORS_BUT_ORANGE],
            'Letters': [...ALL_COLORS],
        },
        textures: {
            masks: {
                BLANKET: 'CCB003.png',
            },
            normals: {
                BLANKET: 'CCB003_normal.png',
            }
        }
    },

    // TODO - remove when ccb003 is added to products and is on LIVE server
    'CCB004': {
        names: {
            'Top Half': 'Top Half Color',
            'Bottom Half': 'Bottom Half Color',
            'Stripe': 'Stripe Color',
        },
        masks: {
            'Top Half': ['BLANKET0'],
            'Bottom Half': ['BLANKET1'],
            'Stripe': ['BLANKET2'],
        },
        colors: {
            'Top Half': [...ALL_COLORS],
            'Bottom Half': [...ALL_COLORS],
            'Stripe': [...ALL_COLORS],
        },
        textures: {
            masks: {
                BLANKET: 'CCB004.png',
            },
            normals: {
                BLANKET: 'CCB004_normal.png',
            }
        }
    },


}

// female sweaters is a copy of male sweaters (unless we need to adjust textures later on)
ITEMS_CONFIG['CCF001'] = ITEMS_CONFIG['CC001'];
ITEMS_CONFIG['CCF002'] = ITEMS_CONFIG['CC002'];
ITEMS_CONFIG['CCF003'] = ITEMS_CONFIG['CC003'];
ITEMS_CONFIG['CCF004'] = ITEMS_CONFIG['CC004'];
ITEMS_CONFIG['CCF005'] = ITEMS_CONFIG['CC005'];
ITEMS_CONFIG['CCF006'] = ITEMS_CONFIG['CC006'];
ITEMS_CONFIG['CCF007'] = ITEMS_CONFIG['CC007'];
ITEMS_CONFIG['CCF008'] = ITEMS_CONFIG['CC008'];
ITEMS_CONFIG['CCF009'] = ITEMS_CONFIG['CC009'];
ITEMS_CONFIG['CCF010'] = ITEMS_CONFIG['CC010'];
ITEMS_CONFIG['CCF011'] = ITEMS_CONFIG['CC011'];
ITEMS_CONFIG['CCF012'] = ITEMS_CONFIG['CC012'];
ITEMS_CONFIG['CCF013'] = ITEMS_CONFIG['CC013'];
ITEMS_CONFIG['CCF014'] = ITEMS_CONFIG['CC014'];

// baby blankets are copy of big blankets just with different handle 
ITEMS_CONFIG['CCBB001'] = ITEMS_CONFIG['CCB001'];
ITEMS_CONFIG['CCBB002'] = ITEMS_CONFIG['CCB002'];
ITEMS_CONFIG['CCBB003'] = ITEMS_CONFIG['CCB003'];
ITEMS_CONFIG['CCBB004'] = ITEMS_CONFIG['CCB004']; // TODO - remove


// locally need these values to display stuff

// men sweaters - CC001
// women sweaters - CCF01
// standart blankets - CCB001
// small blankets - CCBB001


const SWEATER_DEBUG = 'W0001_CC006'; // change here for sweater
const BLANKET_DEBUG = 'CCB001'; // change here for blanket 

// p.s. i got tired of changing 4 lines for sweater...

export const LOCALHOST_FALLBACK = {
    PRODUCT_ID: SWEATER_DEBUG, // toggle this on for sweater
    // PRODUCT_ID: BLANKET_DEBUG, // or toggle this for blanket

    baseURL: "",
    TEXTURES_FOLDER: '../localhost/',
}