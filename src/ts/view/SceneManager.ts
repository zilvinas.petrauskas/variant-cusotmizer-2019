import { PerspectiveCamera, WebGLRenderer, Object3D, AmbientLight, DirectionalLight, Scene } from 'three';

import { Main } from 'Main';
import { ProductSweater } from 'ProductSweater';
import { ProductGarmet } from 'ProductGarmet';
import * as dat from 'three/examples/js/libs/dat.gui.min.js';

const MOBILE_BREAKPOINT = 768; // px

export class SceneManager{

	private _main: Main;
	private _canvas: HTMLCanvasElement;
	private _scene: Scene;
	public _camera: PerspectiveCamera;
	// public _controls: OrbitControls;
	public _renderer: WebGLRenderer;
	public _productLoader: any;
	private _container: Object3D;
	private playGround;
	private paused: boolean = true; // if renderer loop is running or not
	private loop: any; // renderer loop id (used for stopping)
	private assetsLoaded: boolean = false;
	public $controlsParent;
	public $customizerParent;

	// TODO - load all things, but pause renreder [check]
	// TODO - trigger renderer loop (in main.ts) when customization btn is clicked

	constructor(main: Main){

		this._main = main;
		this._canvas = <HTMLCanvasElement>document.getElementById('myCanvas');
		this.playGround = <HTMLCanvasElement>document.querySelectorAll('.playGroundFixed ')[0];
		const width = window.innerWidth / 2;
		const height = window.innerHeight;

		this._canvas.width = width;
		this._canvas.height = height;

		this._scene = new Scene();
		this._container = new Object3D();
		this.initRenderer();

		if(this._main.product.isBlanket()){

			this._productLoader = new ProductGarmet(this, this._main);

		}else{

			this._productLoader = new ProductSweater(this, this._main);
			
		}

		this.onWindowResize();
		if( ! this._main.silentLoaderEnabled) this.animate(0);
		
	}

	private initRenderer(){
		let context = this._canvas.getContext('webgl2');
		// if (!context) context = this._canvas.getContext('experimental-webgl2');
		
		this._renderer = new WebGLRenderer({
			antialias: true,
			canvas: this._canvas,
			context: <WebGLRenderingContext>context
		});
		this._renderer.setPixelRatio(window.devicePixelRatio);
		this._renderer.setClearColor(0xECF8FF);
		this._renderer.gammaOutput = true;

		this._renderer.context.canvas.addEventListener('webglcontextlost', this.onContextLost);

		window.addEventListener('resize', this.onWindowResize);
	}


	public static getObjectByName = (object: Object3D, name: string) =>{

		if(object.name == name) return object;

		for (let child of object.children){

			if( child.type == 'Group' ){
				for(let child2 of <any>child.children){
					if( child2.type == 'Mesh' && ((<any>child2).name == name || (<any>child2).name.indexOf(name) > -1 ) ){
						return child2;
					}
				}
			}else if( child.type == 'Mesh' && (<any>child).name == name ){
				return child;
			}
		}

		return null;
	}


	public controlsVisibilityToggle(visible: boolean){

		// const controlsClassName = 'controls-visible';
		const customizerLoadedClass = 'customizer-loaded';
		const body = document.getElementsByTagName("body")[0]; 

		// console.log('-- change controls visiblity', customizerLoadedClass);

		if(visible === true && !body.classList.contains(customizerLoadedClass)){
			body.classList.add(customizerLoadedClass);
		}else if(visible === false && body.classList.contains(customizerLoadedClass)){
			body.classList.remove(customizerLoadedClass);
		}

	}

	private handleDisableScroll(event){
		event.preventDefault();
		window.scrollTo(0, 0);
	}

	public get camera(){
		return this._camera;
	}

	public get renderer(){
		return this._renderer;
	}

	public get container(){
		return this._container;
	}

	public get scene(){
		return this._scene;
	}

	public get canvas(){
		return this._canvas;
	}

	public onWindowResize = (event?, forcedWidth?, forcedHeight?) =>{
	
		let width = window.innerWidth;
		let height = window.innerHeight;
		if(width > MOBILE_BREAKPOINT) width /= 2;

		const header = document.querySelectorAll('#site-header')[0];
		if(typeof header !== 'undefined')
			height -= header.clientHeight;

		if(typeof forcedWidth !== 'undefined' && forcedWidth > 0) width = forcedWidth;
		if(typeof forcedHeight !== 'undefined' && forcedHeight > 0) height = forcedHeight;
				
		this._canvas.width = width;
		this._canvas.height = height;
		this._renderer.setSize(width, height);

		this._camera.aspect = width / height;
		this._camera.updateProjectionMatrix();
		
	};

	private onContextLost = (event: Event) =>{

		event.preventDefault();
		alert('Unfortunately WebGL has crashed. Please reload the page to continue!');
	};

	public stopRenderer(){

		if(this.paused) return; // already paused

		cancelAnimationFrame(this.loop);
		this.paused = true;
	}

	public queueVisibilityChange(){
		if(this._productLoader.progressBar.finished() ){
			this.controlsVisibilityToggle(true);
		}else{
			this._productLoader.progressBar.queueAutoShow();
		}
		
	}

	public startRenderer(){

		// console.log('-- startRenderer');

		if(!this.paused){
			console.log('- renderer already running', this.paused);
			return; // already running
		}

		// if( ! this._productLoader.progressBar.finished() ) 
		this._productLoader.progressBar.makeVisible();

		this.paused = false;
		this.animate(0);
		
	}

	private update = (time: number) =>{
	
		if(this._productLoader.animate) this._productLoader.animate();
		this._renderer.render(this._scene, this._camera);
	};

	public triggerRender(){ // for screenshot, need to render once before for scene to be visible on canvas
		this.update(0); 
	}

	private animate = (time: number) =>{
		
		this.loop = this._renderer.setAnimationLoop(this.update);
	};
}