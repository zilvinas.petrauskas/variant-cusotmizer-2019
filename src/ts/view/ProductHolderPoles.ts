import { Main } from 'Main';
import { Model } from 'model/Model';
import { Group, BoxBufferGeometry, MeshLambertMaterial, Mesh, Box3, Object3D, Vector3, Color } from 'three';

/*
    README:
    Autodetects product positions and creates poles based on that
    If you need to increase poles' height - simply move product up on y axis
*/

export class ProductHolderPoles {

    private _main: Main;
    private _model: Object3D;
    private _textures: any;
    private _groundConfig: any;
    private poleConfig: any = {
        // height: 460, // dont need height, because its automatically measured
        width: 270, // need width tho
        size: 5, // diameter of pole
        offsetY: 0, // if product should be above horizontal pole, like if its a sweater and its hanging above top pole
    };

    constructor(main: Main, productModel: Object3D, textures: any, groundConfig: any, poleConfig?: any) {

        this._main = main;
        this._model = productModel;
        this._textures = textures;
        this._groundConfig = groundConfig;

        if (poleConfig) {
            // console.log('FOUND POLE CONFIG', poleConfig);
            for (let param in poleConfig) {
                this.poleConfig[param] = poleConfig[param];
                // console.log('CHANGE PARAM', param, poleConfig[param])
            }
        }else{
            // console.log('- NO POLE CONFIG', poleConfig);
        }
    }

    public setConfig(poleConfig: any){
        for (let param in poleConfig) {
            this.poleConfig[param] = poleConfig[param];
        }
    }

    public createPoles() {

        const productInfo = this.getProductDimensions();
        const horizontalPoleY = productInfo.center.y + (productInfo.size.y / 2) + this.poleConfig.offsetY;
        const poleHeight = horizontalPoleY - this._groundConfig.y + this.poleConfig.size;

        const vertPoleY = horizontalPoleY - poleHeight/2 + this.poleConfig.size/2;

        const poles = new Group();

        const poleGeo = new BoxBufferGeometry(5, poleHeight, 5);
        const poleMat = new MeshLambertMaterial({
            map: this._textures.WOOD,
        });

        const castShadow = true;
        const receiveShadow = true;
       

        const horizontalPole = new Mesh(new BoxBufferGeometry(this.poleConfig.width - this.poleConfig.size, this.poleConfig.size, this.poleConfig.size), poleMat);
        horizontalPole.position.x = 0;
        horizontalPole.receiveShadow = receiveShadow;
        horizontalPole.castShadow = castShadow;
        horizontalPole.position.y = horizontalPoleY;
      
        const leftPole = new Mesh(poleGeo, poleMat);
        leftPole.position.x = - this.poleConfig.width / 2;
        leftPole.receiveShadow = receiveShadow;
        leftPole.castShadow = castShadow;
        leftPole.position.y = vertPoleY;
       
        const rightPole = new Mesh(poleGeo, poleMat);
        rightPole.position.x = this.poleConfig.width / 2;
        rightPole.receiveShadow = receiveShadow;
        rightPole.castShadow = castShadow;
        rightPole.position.y = vertPoleY;

        const bottomSize = 10;
        const poleBottom = new BoxBufferGeometry(bottomSize, bottomSize, bottomSize);
        const poleBottomY = this._groundConfig.y;

        const poleStandBottomRight = new Mesh(poleBottom, poleMat);
        poleStandBottomRight.position.x = this.poleConfig.width / 2;
        poleStandBottomRight.position.y = poleBottomY;
        poleStandBottomRight.receiveShadow = true;
        poleStandBottomRight.castShadow = true;

        const poleStandBottomLeft = new Mesh(poleBottom, poleMat);
        poleStandBottomLeft.position.x = - this.poleConfig.width / 2;
        poleStandBottomLeft.position.y = poleBottomY;
        poleStandBottomLeft.receiveShadow = true;
        poleStandBottomLeft.castShadow = true;

        poles.add(horizontalPole);
        poles.add(leftPole);
        poles.add(rightPole);
        poles.add(poleStandBottomRight);
        poles.add(poleStandBottomLeft);

        return poles;
    }

    public getProductDimensions() {

        const box = new Box3().setFromObject(this._model);
        const center = box.getCenter(new Vector3());
        const size = box.getSize(new Vector3());
        // console.log('product center', center);
        // console.log('product size', size);


        return {
            size: size,
            center: center,
        }
    }

}