import { Object3D, Mesh, MeshStandardMaterial, Color, Group, Texture, TextureLoader, CanvasTexture, ShaderMaterial } from 'three';
import { SceneManager } from './SceneManager';
import { PNG2BMPConverter } from 'model/PNG2BMPConverter';
import { HTML } from 'utils/HTML';
import { ITEMS_CONFIG, SWEATER_3D_MODEL, TEXT_STITCH_ENABLED, COLOR_NAMES, ALL_COLORS } from './constants';
import { COLOR_BLACK_SWAN, COLOR_WHITE_COTTON, COLOR_MARINA_BLUE, COLOR_CLASSIC_CAMEL, COLOR_ORANGE, COLOR_FINE_FLANNEL, COLOR_RED_HOT, COLOR_PINE_FOREST } from './constants';
import { Main } from './../Main';
import { PerformanceCheck } from './PerformanceCheck';
import { DynamicPropertiesInjector } from 'utils/DynamicPropertiesInjector';
import { ProductSweater } from './../ProductSweater';
import { ProductGarmet } from './../ProductGarmet';
import { NearestColor } from './NearestColor';


interface itemConfigType {
	names: string[],
	masks: string[],
	colors: string[]
	predefined?: string[],
	customText?: any,
}

export interface IMasks {
	FRT: Texture;
	BCK?: Texture;
	SLV?: Texture;
	BLANKET?: Texture,
}

export class ColorControls {

	private _originalMaskColors: {};
	private _originalImageData: {};
	private _originalNormalData: {};
	public _stitchTextures: any;
	private _activeMaskColors: {};
	private _colorTweakers: {};
	private _containerElement: any;
	private _customizerControls: any;
	private _arrayOfColors: string[];
	private _3DModel: Object3D;
	private _PNG2BMPConverter: PNG2BMPConverter;
	private nearestColor: NearestColor;
	public config: itemConfigType;
	private _main: Main;
	private _product;
	private _activeCustomTexts;
	private activeColorClassName = 'active';
	private _textColors: any = {}; // config has default colors for text, but it may change on the go and here we store them
	private _selectedColors: any = {};
	private _customText: any = {};

	constructor(PNG2BMPConverter: PNG2BMPConverter, object: Object3D, main: Main) {


		this._main = main;
		if (TEXT_STITCH_ENABLED) {
			this._stitchTextures = this._main._scene._productLoader._stitchTextures;
		}
		this._PNG2BMPConverter = PNG2BMPConverter;
		this.nearestColor = new NearestColor(ALL_COLORS);
		this._3DModel = object;
		this._colorTweakers = {};
		this._activeMaskColors = {};
		this._originalMaskColors = {};
		this._originalImageData = {};
		this._originalNormalData = {};
		this._stitchTextures = {};
		this._activeCustomTexts = {};
		this._containerElement = document.getElementById('materialTweaker');

		this._arrayOfColors = [];

		const verified = ['RIB', 'CUF', 'CLR', 'FRT', 'BCK', 'SLV'];

		object.traverse((node: Object3D) => {

			if (node instanceof Mesh) {

				let isVerified = verified.filter(verf => verf == name).length > 0;

				if (isVerified) {
					const material = <MeshStandardMaterial>node.material;
					const name = node.name;
					if (material.map) {
						const canvas = material.map.image;
						this._originalImageData[name] = canvas.getContext('2d').getImageData(0, 0, canvas.width, canvas.height);
					}
				} else {
					//console.log('~~ Not verified ',name, isVerified);
				}
			}
		});

		this.config = this._main.product.getItemConfig();

		// console.log('item config?', this.config);

		if (this.config !== null) {
			this.createCustomControls(this.config);
		} else {
			console.warn('MISSING CONFIG FOR: ' + this._main.product.getProductId(), ITEMS_CONFIG);
		}

	}

	private textureNotFound(err) {
		console.warn('Texture failed to load', err);
	}

	public loadTexture(url: string) {

		return new Promise<Texture>((resolve, reject) => {
			const loader = new TextureLoader();
			loader.load(
				url,
				(texture) => {
					resolve(texture)
				},
				undefined,
				(err) => {
					resolve(null);
				}
			)
		});
	}

	public saveColorsArray(type, arrayOfColors, canvas?) {

		if (canvas) this._originalImageData[type] = canvas.getContext('2d').getImageData(0, 0, canvas.width, canvas.height);

		if (!this._activeMaskColors.hasOwnProperty(type)) this._activeMaskColors[type] = {};
		if (!this._originalMaskColors.hasOwnProperty(type)) this._originalMaskColors[type] = {};

		for (let j = 0; j < arrayOfColors.length; ++j) {
			const maskColor = arrayOfColors[j];
			this._activeMaskColors[type][`color${j + 1}`] = maskColor;
			this._originalMaskColors[type][`color${j + 1}`] = maskColor;
		}

		// console.log('-- _originalMaskColors', type,this._originalMaskColors[type]);
	}

	private createCustomControls(config: itemConfigType) {

		if (config.predefined) {
			this.setLimbColors(config.predefined);
		}

		// attach model parts to array of colors
		for (let name in config.masks) {
			this.createColorPicker2(name, config);
		}

		if (config.customText) {
			for (let i in config.customText) {
				this.createCustomTextField(config.customText[i]);
			}
		}

	}

	private createDynamicProperties() {
		this._main.product.dynamicProperties.setActiveColors(this._selectedColors);
		this._main.product.dynamicProperties.init();
	}

	public detectActiveColors() {
		this.setActiveColorsOnColorPicker(this.config);
	}

	private setActiveColorsOnColorPicker(config) {

		// console.log('detecting active colors:', config);

		const groupsCompleted: any = {};

		// MASKS		
		for (let group in config.masks) {
			for (let i in config.masks[group]) {
				let maskName = config.masks[group][i];
				const part = maskName.substr(0, 1); // F, B, S (for SLV)
				const name = maskName.slice(0, -1); // full name without number
				const index = Number(maskName.substr(-1)) + 1; // number
				let searchFor = '';

				if (name == 'F' || name == 'FRT') {
					searchFor = 'FRT';
				} else if (name == 'B' || name == 'BCK') {
					searchFor = 'BCK';
				} else if (name == 'S' || name == 'SLV') {
					searchFor = 'SLV';
				} else if (name == 'BLANKET') {
					searchFor = 'BLANKET';
				}

				if (searchFor.length > 0) {
					if (this._activeMaskColors[searchFor]) {

						const colorList = this._activeMaskColors[searchFor];
						const colorKey = `color${index}`;

						if (colorList[colorKey]) {
							//console.log('GOT COLOR', group, colorKey, index, colorList[colorKey]);
							// console.log('- SELECT #1', group, colorList[colorKey]);
							this.findAndSetColorPickerElement(group, colorList[colorKey], index);
						}
					}
				}

			}
		}



		// LIMBS (look for _color attribute)
		if (config.predefined) {
			for (let name in config.predefined) {
				let group = this.findMaskGroupName(name, config.masks); // need to find which group this belongs to
				if (group && !groupsCompleted[group]) {
					let mesh = SceneManager.getObjectByName(this._3DModel, this.fixMeshName(name));
					if (mesh) {
						const color = mesh.material._color; // _color
						if (color) {
							// console.log('- SELECT #2', group, group);
							this.findAndSetColorPickerElement(group, color);
							groupsCompleted[group] = color;
						} else {
							console.log('Missing color on config.predefined', color, name, mesh.material, group, config.masks);
						}
					}
				}
			}
		}

		// console.log('saved colors #2', this._selectedColors);

		this.updateDisabledColors();

		// console.log('- check active colors', this._selectedColors);
		this.createDynamicProperties();

	}

	private updateDisabledColors() {
		this.removeDisabledColors();
		for (let name in this._selectedColors) {
			let fixedColor = this._selectedColors[name].toLowerCase();
			this.disableSameColorPick(name, fixedColor);
		}
	}

	private removeDisabledColors() {
		// remove disabled old color
		// const disabledElements = document.querySelectorAll(`#materialTweaker li.disabled[data-color="${oldColor}"]`);
		const disabledClassName = "disabled";
		const disabledElements = document.querySelectorAll(`#materialTweaker li.${disabledClassName}`);
		// console.log('disabled elements?', disabledElements.length);
		if (disabledElements.length > 0) Array.from(disabledElements).map((elem) => { elem.classList.remove(disabledClassName); })

	}

	private findMaskGroupName(name, masks) {

		// console.log('findMaskGroupName', name, masks);

		for (let maskGroupName in masks) {
			for (let indx in masks[maskGroupName]) {
				let maskName = masks[maskGroupName][indx];
				if (maskName.includes(name)) return maskGroupName;
			}
		}

		return null;
	}

	private findAndSetColorPickerElement(group, color, index: number = 0) {

		// console.log('findAndSetColorPickerElement', group, color, index);

		this._selectedColors[group] = color; // need these early in case exported before all colors are touched
		// console.log('- set color for part', group, color);
		// console.log(group, this._selectedColors);


		const groupClassName = this.getColorsListIdName(group);
		let fixedColor = String(color).toLowerCase();

		if(typeof COLOR_NAMES[fixedColor] === 'undefined'){
			let findCorrectColor = this.nearestColor.find(fixedColor);
			console.log('FIXED MISSING COLOR', fixedColor,'->', findCorrectColor);
			if(findCorrectColor){
				fixedColor = String(findCorrectColor).toLowerCase();
			}
		}

		// console.log('NEED COLOR', color, fixedColor);
		const searchFor = `#${groupClassName} li[data-color="${fixedColor}"]`;

		let element = document.querySelectorAll(searchFor)[0];

		if (element) {
			this.setActiveColorElement(element);

		} else {
			console.log('-- MISSING color element for color:', fixedColor, searchFor);
		}
	}

	private setActiveColorElement(element) {
		element.classList.add(this.activeColorClassName);
	}

	private getColorsListIdName(name) {
		let fixedName = String(name).trim().toLowerCase().replace(/ /g, "_"); // replace all spaces with _
		return 'colors_' + fixedName;
	}

	// if in use - add customColors check from product config
	private colorChangeIsAllowed(name, color) {

		const excluded = this.getNonUniquePartsOfModel();

		// check if this._selectedColors[] doesnt have this color, except for its own name
		const isExcluded = excluded.filter(excl => name.includes(excl)).length > 0;
		if (isExcluded) return true;

		let colorChangeAllowed = true;
		for (let key in this._selectedColors) {
			let value = this._selectedColors[key];
			if (isExcluded || (name != key && value == color)) {
				colorChangeAllowed = false;
				break;
			}
		}

		return colorChangeAllowed;
	}

	public exportCustomTextData() {
		return this._customText;
	}

	// excluding rib, cuff, collar
	public exportCustomColors() {

		const excluded = this.getNonUniquePartsOfModel();
		let colorsToExport = {};

		for (let name in this._selectedColors) {
			const isExcluded = excluded.filter(excl => name.includes(excl)).length > 0;
			if (!isExcluded) {

				let colorCode = this._selectedColors[name];
				if(typeof COLOR_NAMES[colorCode] === 'undefined'){
					let _fixColor = this.nearestColor.find(colorCode);
					console.log('COLOR EXPORT FIX', colorCode,'->', _fixColor);
					if(_fixColor) colorCode = _fixColor;
				}

				colorsToExport[name] = colorCode;
			}
		}

		return colorsToExport;
	}

	private createColorPicker2(name: string, config: any) {

		let scope = this;
		const div = document.createElement('div');
		div.classList.add('customizer-line');

		const ul = document.createElement('ul');
		ul.setAttribute('id', this.getColorsListIdName(name));

		const heading = document.createElement('h3');
		heading.innerHTML = config.names[name] || name.toUpperCase();

		for (var i = 0; i < config.colors[name].length; i++) {

			const color = config.colors[name][i];
			const colorElement = document.createElement('li');
			colorElement.style.backgroundColor = `#${color}`;
			colorElement.setAttribute('data-color', color);
			colorElement.setAttribute('data-index', String(i + 1));

			colorElement.addEventListener('click', (event: PointerEvent) => {

				// if( ! scope.colorChangeIsAllowed(name, color)){
				// 	const msg = 'U3VjaCBoYWNraW5nIHNraWxscyEgTk9QRSE=';
				// 	try{ alert(window.atob(msg)); }catch(e){};
				// 	return; 
				// }	

				HTML.removeFromClassList(colorElement.parentElement.children, this.activeColorClassName);
				this.setActiveColorElement(colorElement);

				// console.log('change color of', name, color);
				scope.updateModelColors(
					color,
					config.masks[name],
					scope._PNG2BMPConverter.changeColorOfModel.bind(scope),
					scope.changeTextureOnModel.bind(scope),
					scope.updateCustomTextColor.bind(scope)
				);

				this._main.product.productModified();

				this._selectedColors[name] = color;
				// also update customText color if it follows this part
				this.checkAndUpdateFollowedAreaColor(name, color, config);

				this.updateDisabledColors();

				scope._main.product.dynamicProperties.setActiveColors(this._selectedColors);
				scope._main.product.dynamicProperties.updateColorInput(name, color);

			});

			ul.appendChild(colorElement);

		}

		div.appendChild(heading);
		div.appendChild(ul);

		this._containerElement.appendChild(div);

	}

	// ONLY FOR CUSTOM TEXT
	protected checkAndUpdateFollowedAreaColor(name, color, config){

		if( ! config.hasOwnProperty('customText') ) return;

		const partsArray = config.masks[name];

		for(let i=0;i<config.customText.length;i++){
			let customText = config.customText[i];
			if(customText.hasOwnProperty('follow') && partsArray.includes( customText.follow )){
				this._main.product.dynamicProperties.updateTextColor(customText.name, color); // qq2
			}
		}
		
	}


	private getNonUniquePartsOfModel() {
		return ['Rib', 'Cuf', 'Collar']; // must be a part of keys in product config 'masks'
	}

	// so just go thru all fields (except for rib, cuf, clr) and do not allow duplciate colors
	private disableSameColorPick(type, color) {

		const _debug = !!0;

		if (_debug) console.log('------------------------');
		if (_debug) console.log('-- check', type, COLOR_NAMES[color]);

		if( ! COLOR_NAMES.hasOwnProperty(color) ){
			color = this.nearestColor.find(color);
		}

		const excluded = this.getNonUniquePartsOfModel();
		const disabledClassName = 'disabled';
		const oldColor = this._selectedColors[type];
		const differentFrom = []; // partName -> not_same_color_as_parts_array?
		var typeLower = type.toLowerCase();

		// console.log('-excluded parts', excluded);

		// TODO - have a custom options to include/exclude more than non unique parts 
		const config = this._main.product.getItemConfig();

		if (config.customRules) {
			for (let part in config.customRules) {

				var partName = part.toLowerCase();
				const custom = config.customRules[part];

				// make sure part is unique if defined so (meaning other elements cant have same color)
				if (custom.unique === true) {
					for (let e in excluded) {
						let index = parseInt(e);
						let exludedPart = excluded[index].toLowerCase();
						if (partName == exludedPart || partName.indexOf(exludedPart) > -1) {
							excluded.splice(index, 1);
							console.log(`--- remove ${partName} from excluded`, excluded);
						}
					}
				}

				if (custom.differentFrom) { // find parts in array that matches 'type' and disable their specific color
					for (let p in custom.differentFrom) {

						let otherPart = custom.differentFrom[p].toLowerCase();
						// console.log(' - compare ', partName, typeLower);
						if (partName == typeLower) {
							differentFrom.push(otherPart);
							if (_debug) console.log('---> ADD --->', typeLower, color, 'disable -> ', otherPart, color);
						}
					}
				}
			}
		}


		this._selectedColors[type] = color;

		// remove disabled old color
		// const disabledElements = document.querySelectorAll(`#materialTweaker li.disabled[data-color="${oldColor}"]`);
		// if (disabledElements.length > 0) Array.from(disabledElements).map((elem) => { elem.classList.remove(disabledClassName); })

		var secondCheck = this.getNonUniquePartsOfModel();
		var affectsAllOtherParts = !(secondCheck.filter(excl => type.includes(excl)).length > 0);

		if (_debug) console.log('- affects all', affectsAllOtherParts, type);
		if (_debug) console.log('- debug', type, affectsAllOtherParts, excluded);

		for (let name in this.config.masks) {

			if (name == type) {
				if (_debug) console.log('- SKIP', name, 'because of', type);
				continue; // does not affect current part which color is beeing changed
			}

			let nameLower = name.toLowerCase();

			// parts like collar, rib, cuff can be any color
			let exclude = (excluded.filter(excl => name.includes(excl)).length > 0);
			if (_debug) console.log('- excluded?', nameLower, exclude);
			// UNLESS this partName is in differentFrom array, then it SHOULD be disabled
			if (exclude && differentFrom.length > 0) {
				let denyExclude = (differentFrom.filter(d => d == nameLower)).length > 0;
				if (denyExclude) exclude = false;

				if (_debug) console.log('- deny?', nameLower, denyExclude);
				if (_debug) console.log('- WHY?', nameLower, typeLower, differentFrom);
			}

			if (!exclude) {

				if (affectsAllOtherParts) { // if current part (for which color is changed) can have affect on all other parts, then do so
					let colorGroupId = this.getColorsListIdName(name);
					let elems = document.querySelectorAll(`#${colorGroupId} li[data-color="${color}"]`);
					if (elems.length > 0) Array.from(elems).map((elem) => { elem.classList.add(disabledClassName); });
				} else {
					// only disable from differentFrom array
					for (let i = 0; i < differentFrom.length; i++) {
						let colorGroupId = this.getColorsListIdName(differentFrom[i]);
						if (_debug) console.log('- ONLY DISABLE', colorGroupId, differentFrom[i]);
						let elems = document.querySelectorAll(`#${colorGroupId} li[data-color="${color}"]`);
						if (elems.length > 0) Array.from(elems).map((elem) => { elem.classList.add(disabledClassName); });
					}
				}
			}
		}
	}


	private updateModelColors(
		color: string,
		models: string[],
		changeColor: Function,
		changeTexture: Function,
		changeCustomTextColor: Function) {

		// changeTexture - for canvas based textures/images
		// changeColor - for 3d model parts


		for (let modelName of models) {

			switch (modelName) {

				case 'F0': changeTexture('FRT', color);
					break;
				case 'B0': changeTexture('BCK', color);
					break;
				case 'SLV0': changeTexture('SLV', color);
					break;
				case 'CLR0': 	//changeColor('CLR_0',color); //backup
					//changeColor('CLR_1', color); //backup
					changeColor('CLR', color);
					break;
				case 'RIB0':	//changeColor('RIB_0', color);  //backup
					//changeColor('RIB_1', color); //backup
					changeColor('RIB', color);
					break;
				case 'CUF0': changeColor('CUF', color);
					break;

				case 'F1':
				case 'F2':
				case 'F3':
				case 'F4':
				case 'F5':
					const frontIndex = parseInt(modelName.slice(-1));
					changeTexture('FRT', color, frontIndex);
					break;

				case 'B1':
				case 'B2':
				case 'B3':
				case 'B4':
				case 'B5':
					const backIndex = parseInt(modelName.slice(-1));
					changeTexture('BCK', color, backIndex);
					break;

				case 'SLV1':
				case 'SLV2':
				case 'SLV3':
				case 'SLV4':
				case 'SLV5':
					const sleeveIndex = parseInt(modelName.slice(-1));
					changeTexture('SLV', color, sleeveIndex);
					break;

				case 'BLANKET0':
				case 'BLANKET1':
				case 'BLANKET2':
				case 'BLANKET3':
				case 'BLANKET4':
				case 'BLANKET5':
					const blanketIndex = parseInt(modelName.slice(-1));
					changeTexture('BLANKET', color, blanketIndex, true);
					break;

				// case 'BLANKET_TOP_TEXT': changeCustomTextColor('BLANKET', color, modelName); break;
				// case 'BLANKET_BOT_TEXT': changeCustomTextColor('BLANKET', color, modelName); break;

				case 'FRT_TEXT': changeCustomTextColor('FRT', color, modelName); console.log(color, modelName); break;
				case 'BCK_TEXT': changeCustomTextColor('BCK', color, modelName); break;
				case 'SLV_TEXT': changeCustomTextColor('SLV', color, modelName); break;

				default: console.log('--- MISSING A CASE FOR:', modelName);
					break;

			}

		}


	}

	private setLimbColors(predefined: any) {

		if (predefined.RIB) { // there are actually 2 models for RIB
			// this._PNG2BMPConverter.changeColorOfModel('RIB_0', predefined.RIB);  //backup
			// this._PNG2BMPConverter.changeColorOfModel('RIB_1', predefined.RIB);  //backup
			this._PNG2BMPConverter.changeColorOfModel('RIB', predefined.RIB);
			// console.log('SET LIMB COLOR #RIB', predefined.RIB);
		}

		if (predefined.CLR) { // and two models (back and front) for COLLAR
			// this._PNG2BMPConverter.changeColorOfModel('CLR_0', predefined.CLR);  //backup
			// this._PNG2BMPConverter.changeColorOfModel('CLR_1', predefined.CLR);  //backup
			this._PNG2BMPConverter.changeColorOfModel('CLR', predefined.CLR);
			// console.log('SET LIMB COLOR #CLR', predefined.CLR);
		}

		if (predefined.CUF) { // only one model for cuff
			this._PNG2BMPConverter.changeColorOfModel('CUF', predefined.CUF);
			// console.log('SET LIMB COLOR #CUF', predefined.CUF);
		}

	}


	private changeTextureOnModel(name: string, color: string, index: number = 0, checkTextColor: boolean = false) {


		let fixedName = this.fixMeshName(name);
		// console.log('change texture for', name, fixedName, 'color: '+color, 'index: ',index);
		// console.log('-- change color on texture', name, index, color);
		checkTextColor = true;

		const mesh = SceneManager.getObjectByName(this._3DModel, fixedName);
		if (mesh) {
			if (mesh.material.map) {

				if (checkTextColor) {

					this.checkForTextColorChange(name, index, color);
				}

				mesh.material.color.set(0xFFFFFF); // reset?
				this.swapColorsOnTexture(mesh.material.map, color, name, index);



			} else {
				console.log('--Missing material.map for', name, mesh);
			}
		} else {
			console.log('--CANT CHANGE NO MAP', name, fixedName, mesh, this._3DModel);
		}

	}

	public checkForTextColorChange(name: string, index: number, color: string) {

		// need config FOR THIS

		// qq1


		const config = this.config;
		const fullName = name + '' + index; //

		// console.log('-- CHECK', fullName, name, index, color);

		if (config.customText) {
			for (let i = 0; i < config.customText.length; i++) {
				if (config.customText[i].hasOwnProperty('follow')) {
					let follow = config.customText[i].follow;

					// console.log('-- checking..', follow, fullName, 'relation: ', this.getPositionRelation(config.customText[i].position));



					if (fullName == follow) {

						let relation = this.getPositionRelation(config.customText[i].position);
						this._textColors[relation] = color; // done & ready for next usage
						// console.log('FOUND RELATION', fullName, '->', relation, 'color:', color, this._textColors);

					}
				}
			}
		}

	}

	private searchModelByTextureName(name: string) {


		for (let child of this._3DModel.children) {

			if ((<any>child) instanceof Group) {
				for (let child2 of <any>child) {
					if ((<any>child) instanceof Mesh && (<any>child2).material.name.toLowerCase() === name.toLowerCase()) {
						return child2;
					}
				}
			} else if ((<any>child) instanceof Mesh) {
				if ((<any>child) instanceof Mesh && (<any>child).material.name.toLowerCase() === name.toLowerCase()) {
					return child;
				}
			}
		}

		return null;
	}

	private getPositionRelation(position) {

		let relation = '';

		switch (position) {
			case 'F':
			case 'F0':
			case 'FRT':
			case 'FRT_TEXT':
				relation = 'FRT_TEXT';
				break;
			case 'B':
			case 'B0':
			case 'BCK':
			case 'BCK_TEXT':
				relation = 'BCK_TEXT';
				break;
			case 'SLV':
			case 'SLV0':
			case 'SLV_TEXT':
				relation = 'SLV_TEXT';
				break;
			case 'BLANKET_TOP':
			case 'BLANKET_TOP_TEXT':
				relation = 'BLANKET_TOP_TEXT';
				break;
			case 'BLANKET_BOT':
			case 'BLANKET_BOT_TEXT':
				relation = 'BLANKET_BOT_TEXT';
				break;
		}

		return relation;
	}



	private createCustomTextField(config) {

		const div = document.createElement('div');
		div.classList.add('customizer-line');
		const heading = document.createElement('h3');

		if (config.title) {
			heading.innerHTML = config.title;
		} else if (config.required === true) {
			heading.innerHTML = 'Text (Required)';
		} else {
			heading.innerHTML = 'Text (Optional)';
		}

		let defaultColor = 'FFFFFF';
		if (config.color) defaultColor = config.color;

		let relation = this.getPositionRelation(config.position);
		this._textColors[relation] = defaultColor;


 
		this.saveColorsArray(relation, [defaultColor]);

		this._containerElement.appendChild(div);

		const txtInput = document.createElement('input');
		txtInput.classList.add('input');

		let maxTextLength = 1;

		if (config.limit) {
			maxTextLength = config.limit;
			txtInput.placeholder = `Enter up to ${config.limit} symbols`;
			txtInput.maxLength = config.limit;
		} else {
			txtInput.placeholder = `Enter 1 symbol`;
			txtInput.maxLength = 1;
		}

		let defaultText = '';
		if (config.hasOwnProperty('value') && config['value'].length > 0) {
			defaultText = config['value'];
		}

		this._customText[config.name] = defaultText;

		txtInput.value = defaultText;

		const relatedMeshType = this.getTextRelationToMesh(relation);
		// console.log('relatedMeshType',relatedMeshType);

		let prevInputValue = defaultText;

		txtInput.addEventListener('change', () => {

			let text = txtInput.value;

			// let cleanText = text.replace(/[^0-9a-z]/gi, ''); // only alpha numerics are allowed
			let cleanText = text.replace(/[^\w\s]/gi, ''); // alphanumeric and spaces allowed

			if (text != cleanText) {
				txtInput.value = cleanText;
				text = cleanText;
			}

			if (prevInputValue == text) return; // no changes

			text = text.toUpperCase();

			prevInputValue = text;

			if (text.length > maxTextLength) text = text.substring(0, maxTextLength); // in case user edited html we want to force max length in here

			const name = this.fixMeshName(relatedMeshType);
			const mesh = SceneManager.getObjectByName(this._3DModel, name);

			if (mesh && (<any>mesh).material.map) {

				// if text has relation to other model part (customText.follow) then use its color
				if (config.hasOwnProperty('follow')) {
					const cleanName = config.follow.replace(/[0-9]/g, ''); // in case something like CLR0 is supplied, remove 0, because in 3d model, names are without indexes
// <<<<<<< Updated upstream

					if (cleanName === 'CLR' || cleanName === 'CUF' || cleanName == 'RIB') {
						// look for mesh color
						const mesh = <any>SceneManager.getObjectByName(this._3DModel, cleanName);
						if (!!mesh) {
							this._textColors[config.position] = mesh.material._color;
						}

					} else {
						// look for color on texture
						let colorPosition = Number( config.follow.match(/\d+/g).map(Number) ) + 1;
						if (this._activeMaskColors[cleanName] && this._activeMaskColors[cleanName][`color${colorPosition}`]) {
							// this._textColors[config.position] = this._activeMaskColors[cleanName][`color${colorPosition}`]; // match texture color to text
							this._textColors[config.position] = this._originalMaskColors[cleanName][`color${colorPosition}`]; // match texture color to text
						}
// =======
					// const mesh = <any>SceneManager.getObjectByName(this._3DModel, cleanName);
					
					// if (!!mesh) {
// 						color = this._textColors[relation];
						// this._textColors[config.position] = mesh.material._color;
						// this._textColors[relation] = this._textColors[config.position];
// >>>>>>> Stashed changes
					}

					// console.log('_activeMaskColors', cleanName, this._activeMaskColors, this._originalMaskColors);

				}


				// console.log('change to color', COLOR_NAMES[color], config.position, defaultColor);
				let color = this._textColors[config.position] ? this._textColors[config.position] : defaultColor;


				let textConfig: any = {
					color: color,
					type: relatedMeshType,
					position: config.position,
				}

				if (config.y) textConfig.y = config.y;
				if (config.x) textConfig.x = config.x;

				// console.log('config used?', config);
				// console.log('change text on input', name, config);
				this._customText[config.name] = text;

				this._main.product.dynamicProperties.updateTextInput(config.name, text, this._customText);
				this._main.product.productModified();

				this.saveTextPlacement(relatedMeshType, relation, text, textConfig);

				// console.log('change text color' ,textConfig);

				this._main.product.dynamicProperties.updateTextColor(config.name, textConfig.color);


				this.changeTextOnMaterial(text, textConfig, (<any>mesh).material.map);


			} else {
				console.log('no mesh found for ', relatedMeshType);
			}

		});

		div.appendChild(heading);
		div.appendChild(txtInput);

		this._containerElement.appendChild(div);

	}

	// this should only be called on load (init), for user interaction text change use listerner in createCustomTextField() fn
	public applyTextOnModel(config) {

		const relation = this.getPositionRelation(config.position)
		const relatedMeshType = this.getTextRelationToMesh(relation);

		const text = config.value;
		let color = this._textColors[config.position] ? this._textColors[config.position] : config.color;

		const mesh = SceneManager.getObjectByName(this._3DModel, this.fixMeshName(relatedMeshType));

		let textConfig: any = {
			color: color,
			type: relatedMeshType,
			position: config.position,
		}
		if (config.y) textConfig.y = config.y;
		if (config.x) textConfig.x = config.x;

		this._customText[config.name] = text;
		// this._main.product.dynamicProperties.updateTextInput(config.name, text, this._customText);
		this.saveTextPlacement(relatedMeshType, relation, text, textConfig);
		// this._main.product.dynamicProperties.updateTextColor(config.name, textConfig.color);

		this.changeTextOnMaterial(text, textConfig, (<any>mesh).material.map);

	}

	private updateCustomTextColor(meshName, color, relation) {

		const config = this.config.customText.filter(c => c.position == meshName)[0];

		if (config) {

			const mesh = SceneManager.getObjectByName(this._3DModel, this.fixMeshName(meshName));

			if (mesh && (<any>mesh).material.map) {

				const activeTextConfig = this._activeCustomTexts[relation];
				// activeTextConfig.config.color = this._textColors[config.position];

				console.log('- activeTextConfig ', activeTextConfig, relation);

				if (config.hasOwnProperty('follow')) {

					this._textColors[config.position] = color;
					this._textColors[relation] = color;
					// this._activeCustomTexts[]

				}

				// WATCH OUT: this part is different from create text inputs


				if (activeTextConfig) { // may not exist

					const passConfig = {
						color: color,
						type: meshName,
						position: meshName,
					}

					this.saveTextPlacement(meshName, relation, activeTextConfig.text, passConfig);
					this.changeTextOnMaterial(activeTextConfig.text, passConfig, (<any>mesh).material.map);
					// console.log(config);
					// this.swapColorsOnTexture(mesh.material.map, color, name);
				}


			} else {
				console.log('no mesh found on updateCustomTextColor for ', meshName, relation);
			}

		} else {
			console.log('-- FAILED at updateCustomTextColor', meshName, color, relation, ' IN ', this.config.customText);
		}





	}

	// there are multiple materials with same material name, but object names are unique. Problem with object.name is that for examlple shirt object is a group, where childrens named shirt_0, shirt_1, etc
	public fixMeshName(name) {

		let fixedName = name;

		if (!SWEATER_3D_MODEL.includes('new_')) {
			switch (name) { // because in 3d model object is named differently.
				case 'BCK': fixedName = 'shirt_0'; break;  //backup
				case 'SLV': fixedName = 'shirt_1'; break;  //backup
				case 'FRT': fixedName = 'shirt_2'; break;  //backup
			}
		}

		// console.log('fixed name?', name,'->', fixedName);
		return fixedName;
	}

	// FRT_TEXT -> FRT,
	// BCK_TEXT -> BCK
	private getTextRelationToMesh(relation) {
		return relation.split('_').shift();
	}

	private static componentToHex = (c: number) => {
		const hex = c.toString(16).toUpperCase();

		return hex.length == 1 ? "0" + hex : hex;
	};

	public static rgbToHex = (r: number, g: number, b: number) => {
		return '' + ColorControls.componentToHex(r) + ColorControls.componentToHex(g) + ColorControls.componentToHex(b);
	};

	public static hexToRGB = (hex) => {
		const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
		return result ? {
			r: parseInt(result[1], 16),
			g: parseInt(result[2], 16),
			b: parseInt(result[3], 16)
		} : null;
	}

	private getOriginalImageDataFor(type) {
		return this._originalImageData[type];
	}

	// IMPORTANT TO UNDERSTAND: FIRST COLOR IN MASKS LIST IS ALWAYS WHITE (until changed) for a text (when its being changed)

	public changeTextOnMaterial(text, config, map) {

		console.log(config);

		let relation = this.getPositionRelation(config.position);
		const activeTextConfig = this._activeCustomTexts[relation];

		const productConfig = this._main.product.getItemConfig();
		const maskType = config.type;

		// create canvas with default texture on it
		const [canvas, ctx] = this.createBlankCanvas(map);
		ctx.putImageData(this.getOriginalImageDataFor(maskType), 0, 0);

		// console.log('-- CHANGE text on material', config);

		// then change/add text on top of everything previuosly done


		if (productConfig.customText) {


			var stitchType;

			if (TEXT_STITCH_ENABLED) stitchType = this.getStitchType(config.position);
			const changedText = this.drawText(text, config, canvas, ctx, stitchType);
			const changedText2 = this.checkForAdditionalActiveTexts(this.getPositionRelation(config.position), canvas, ctx);
			
			if (changedText || changedText2) {
				map.needsUpdate = true;
			}

		}

		const changedTexture = this.replaceModifiedColorsWithShaders(canvas, ctx, maskType);

		this.updateMaterialWithTextAndMask(this._3DModel, maskType, changedTexture);

		if (changedTexture) {
			map.needsUpdate = true;
		}



	}

	private drawText(text, config, canvas, ctx, stitch?) {

		console.log(config.color);

		// canvas.style.fontSmooth = "never";

		const heightRatio = canvas.height / 528; // the original solution was made using a 528 high canvas in mind
		//const widthRatio = canvas.width / 318; // the original solution was made using a 528 wide canvas in mind

		let rotated = false;

		let _x = 1, _y = 1; // _x && _y are percentage values, x & y needs to be computed into pixel values before passing into ctx.fillText()
		let lockedPosition = false; // if x,y values are coming from config, then dont use default values for each type
		let lockedX = false;
		let lockedY = false;

		if (config.x) {
			_x = config.x;
			lockedX = true;
		}

		if (config.y) {
			_y = config.y;
			lockedY = true;
		}

		let fontSize = 50;
		if (config.fontSize) fontSize = config.fontSize;

		let fontFamily = 'Arial';
		if (config.fontFamily) fontFamily = config.fontFamily;

		let fontWeight = 'bold';
		if (config.fontWeight) fontWeight = config.fontWeight;

		let textAlign = 'center';
		if (config.textAlign) fontWeight = config.textAlign;

		// default positions based on mask type if config.type (mask type) is supplied
		if (config.type) {
			switch (config.type) {
				case 'FRT': if (!lockedX) _x = 75.5;
					if (!lockedY) _y = 37;
					break;
				case 'SLV':
					if (!lockedX) _x = 48;
					if (!lockedY) _y = 80;

					if (!config.fontSize) fontSize = 40;
					if (!config.textAlign) textAlign = 'left';
					break;
				case 'BCK':
					if (!lockedX) _x = 50;
					if (!lockedY) _y = 30;

					if (!config.textAlign) textAlign = 'left';
					break;
				case 'BLANKET':
					if (!lockedX) _x = 50;
					if (!lockedY) _y = 93;

					if (config.y) _y = config.y;
					if (!config.fontSize) fontSize = 30;
					if (config.position && config.position == 'BLANKET_TOP') { // BLANKET_TOP | BLANKET_BOT
						rotated = true; // when text is rotated, we can keep same _y because it will be calculated from other side
					}
					if (!config.textAlign) textAlign = 'left';
					break;
			}
		}

		let computedFontSize = fontSize * heightRatio;
		let normalComputedFontSize = computedFontSize * 2;

		ctx.font = `${fontWeight} ${computedFontSize}px ${fontFamily}`;
		ctx.textAlign = textAlign;
		ctx.textBaseline = 'middle';
		ctx.fillStyle = `#${config.color}`;
	    // ctx['imageSmoothingEnabled'] = false;       /* standard */
	    // ctx['mozImageSmoothingEnabled'] = false;    /* Firefox */
	    // ctx['oImageSmoothingEnabled'] = false;      /* Opera */
	    // ctx['webkitImageSmoothingEnabled'] = false; /* Safari */
	    // ctx['msImageSmoothingEnabled'] = false;     /* IE */

		// Text overlay on Normal Map

		let mesh = SceneManager.getObjectByName(this._3DModel, config.type);
		// WILLIAM - it might not exist (and it doesnt) on product init

		const measured = {
			width: ctx.measureText(text).width,
			height: computedFontSize,
		}

		const computed = {
			x: this.getPercentagePosition(_x, canvas.width, measured.width || 0),
			y: this.getPercentagePosition(_y, canvas.height),
		}

		if (rotated) {
			ctx.save();
			ctx.translate(canvas.width, canvas.height);
			ctx.rotate(Math.PI); // rotate for upside down text
		}

		// ctx.fillStyle = "#" + config.color;
		ctx.fillText(text, computed.x, computed.y);

		if (rotated) {
			ctx.restore(); // rotate back
		}

		// stitch texture for custom text
		let stitchNormalImage = null;
		let normalImage = null;
		let normalCanvas: any = null;
		var normalCtx;

		if (mesh && mesh.material.normalMap) {
			normalImage = mesh.material.normalMap.image;
			normalCanvas = document.getElementById('NORMAL_' + config.type);
		}

		if (TEXT_STITCH_ENABLED && normalImage !== null) {

			if (config && stitch) {
				let stitchNormal = this._stitchTextures[stitch]; // its not loaded yet on customizer's load
				if (typeof stitchNormal !== 'undefined' && stitchNormal !== null) {
					stitchNormalImage = stitchNormal.image;
				}
			}

			if (normalCanvas == null) {
				normalCanvas = document.createElement('canvas');
				normalCanvas.width = normalImage.width;
				normalCanvas.height = normalImage.height;
				normalCanvas.id = 'NORMAL_' + config.type;
				normalCanvas.style.zIndex = "1000";
				normalCanvas.style.position = "absolute";
				normalCanvas.style.display = "none";
				normalCanvas.style.top = "0";
				document.body.appendChild(normalCanvas);
				this._originalNormalData[config.type] = normalImage;

			} else {
				normalImage = this._originalNormalData[config.type];
			}

			normalCtx = normalCanvas.getContext('2d');
			normalCtx.drawImage(normalImage, 0, 0);
			normalCtx.font = `${fontWeight} ${normalComputedFontSize}px ${fontFamily}`;
			normalCtx.textAlign = textAlign;
			normalCtx.textBaseline = 'middle';

			// WILLIAM x3 ! ADD IFs until feature is 100% complete and working!
			// I'm making changes on sunday and have to deal with this on top of everything else
			if (stitchNormalImage !== null) {

				let stitchNormalPattern = normalCtx.createPattern(stitchNormalImage, "repeat");
				normalCtx.fillStyle = stitchNormalPattern;
				normalCtx.fill();

			}

			const normalMeasured = {
				width: normalCtx.measureText(text).width,
				height: computedFontSize,
			}

			const normalComputed = {
				nX: this.getPercentagePosition(_x, normalCanvas.width, normalMeasured.width || 0),
				nY: this.getPercentagePosition(_y, normalCanvas.height),
			}

			if (rotated) {
				normalCtx.save();
				normalCtx.translate(normalCanvas.width, normalCanvas.height);
				normalCtx.rotate(Math.PI); // rotate for upside down text
			}

			normalCtx.fillText(text, normalComputed.nX, normalComputed.nY);

			if (rotated) {
				normalCtx.restore();
			}

			var stitchNormalTexture = new CanvasTexture(normalCanvas);
			stitchNormalTexture.flipY = false;
			mesh.material.normalMap = stitchNormalTexture;
			mesh.material.needsUpdate = true;
		}

		// console.log('draw text:',text,'at positions', computed, {width: canvas.width, height: canvas.height});

		// return canvas;
		return true;

	}

	private getPercentagePosition(percentage: number, size: number, itemSize?: number) { // size2 is for centereing
		let position = size / 100 * percentage;
		if (itemSize && itemSize != 0) position -= itemSize / 2; // centered position based on itemSize
		return position;
	}


	private replaceModifiedColorsOnTexture(canvas, ctx, maskType) {

		const originalMaskColors = this.getAllColorsOnOriginalMask(maskType);
		const activeMaskColors = this.getAllColorsOnActiveMask(maskType);
		const maskColorCount = this.countColorsForMask(activeMaskColors);

		let changed = 0;

		// let perf1 = new PerformanceCheck('color change');
		// let perf2;

		for (let j = 1; j <= maskColorCount; j++) {

			const originalColor = originalMaskColors[`color${j}`]; // original mask color at index
			const activeColor = activeMaskColors[`color${j}`];
			const newColorInRGB = ColorControls.hexToRGB(activeColor); // active mask color at index
			const imgData = ctx.getImageData(0, 0, canvas.width, canvas.height);

			// console.log('for #'+j, originalColor, newColorInRGB);

			// console.log('reading data length', imgData.data.length);

			for (let i = 0; i < imgData.data.length; i += 4) { // checks each pixel for rgba values (red, green, blue, opacity)
				const r = imgData.data[i];	// red channel
				const g = imgData.data[i + 1]; // green channel
				const b = imgData.data[i + 2]; // blue channel
				const a = imgData.data[i + 3]; // transparency

				if (ColorControls.rgbToHex(r, g, b) === originalColor) {
					imgData.data[i] = newColorInRGB.r;
					imgData.data[i + 1] = newColorInRGB.g;
					imgData.data[i + 2] = newColorInRGB.b;
					changed++;
				}
			}



			// if(j==1) perf2 = new PerformanceCheck('write color on canvas');

			ctx.putImageData(imgData, 0, 0); // TODO - do we need to update this often or can we use imgData for whole process?

			// if(j==1) perf2.stop();

		}

		return changed;
	}

	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// ----------------------- COLOR DETECTOR EXPERIMENT -----------------------
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
	// * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *

	private replaceModifiedColorsWithShaders(canvas, ctx, maskType) {

		const originalMaskColors = this.getAllColorsOnOriginalMask(maskType);
		const activeMaskColors = this.getAllColorsOnActiveMask(maskType);
		const maskColorCount = this.countColorsForMask(activeMaskColors);

		let changed = 0;

		var glCanvas = this.getTextureInfo(canvas, this.config, originalMaskColors, activeMaskColors, maskType, maskColorCount, this._3DModel);

		return glCanvas;
	}


	private getTextureInfo(canvas, config, originalMaskColors, activeMaskColors, maskType, maskColorCount, model) {


		const VERTEX = `
			attribute vec2 a_position;
			attribute vec2 a_texCoord;

			uniform vec2 u_resolution;

			varying vec2 v_texCoord;

			void main() {
			   // convert the rectangle from pixels to 0.0 to 1.0
			   vec2 zeroToOne = a_position / u_resolution;

			   // convert from 0->1 to 0->2
			   vec2 zeroToTwo = zeroToOne * 2.0;

			   // convert from 0->2 to -1->+1 (clipspace)
			   vec2 clipSpace = zeroToTwo - 1.0;

			   gl_Position = vec4(clipSpace * vec2(1, -1), 0, 1);

			   // pass the texCoord to the fragment shader
			   // The GPU will interpolate this value between points.
			   v_texCoord = a_texCoord;
			}
		`

		const FRAGMENT = `
			precision highp float;
			uniform vec2 resolution;

			// our texture
			uniform sampler2D u_image;

			// the texCoords passed in from the vertex shader.
			varying vec2 v_texCoord;


			uniform int u_maskCount;

			uniform vec4 u_color1;
			uniform vec4 u_replace1;
			uniform vec4 u_color2;
			uniform vec4 u_replace2;
			uniform vec4 u_color3;
			uniform vec4 u_replace3;
			uniform vec4 u_color4;
			uniform vec4 u_replace4;
			uniform vec4 u_color5;
			uniform vec4 u_replace5;

			uniform vec4 u_textColor;
			uniform vec4 u_textReplace;

			void main() {

				vec4 eps = vec4(0.01, 0.01, 0.01, 0.0);
				vec4 pixel = texture2D(u_image, v_texCoord);

				if( all( greaterThanEqual(pixel, vec4(u_color1 - eps)) ) && all( lessThanEqual(pixel, vec4(u_color1 + eps)) ) )
				{
					pixel = vec4(u_replace1);
				}
				else if( all( greaterThanEqual(pixel, vec4(u_color2 - eps)) ) && all( lessThanEqual(pixel, vec4(u_color2 + eps)) ) )
				{
					pixel = vec4(u_replace2);
				}
				else if( all( greaterThanEqual(pixel, vec4(u_color3 - eps)) ) && all( lessThanEqual(pixel, vec4(u_color3 + eps)) ) )
				{
					pixel = vec4(u_replace3);
				}
				else if( all( greaterThanEqual(pixel, vec4(u_color4 - eps)) ) && all( lessThanEqual(pixel, vec4(u_color4 + eps)) ) )
				{
					pixel = vec4(u_replace4);
				}
				else if( all( greaterThanEqual(pixel, vec4(u_color5 - eps)) ) && all( lessThanEqual(pixel, vec4(u_color5 + eps)) ) )
				{
					pixel = vec4(u_replace5);
				}

				else if( all( greaterThanEqual(pixel, vec4(u_textColor - eps)) ) && all( lessThanEqual(pixel, vec4(u_textColor + eps)) ) )
				{
					pixel = vec4(u_textReplace);
				}

				gl_FragColor = pixel;
			}
		`

		"use strict";

		function main() {
			var image = new Image();
			image = canvas.getContext('2d').getImageData(0, 0, canvas.width, canvas.height);
			render(image);

			var glCanvas = render(image);

			return glCanvas;
		}


		function render(image) {

			// create empty canvas
			var tempCanvas = <HTMLCanvasElement>document.getElementById(maskType);

			var baseArrays;
			var colorArrays;

			if (tempCanvas != null) {
			} else {
				tempCanvas = document.createElement('canvas');
				tempCanvas.id = maskType;
				tempCanvas.style.zIndex = "1000";
				tempCanvas.style.position = "absolute";
				// if (maskType != "FRT") {
				tempCanvas.style.display = "none";
				// }
				tempCanvas.style.top = "0";
				document.body.appendChild(tempCanvas);
				tempCanvas.width = image.width;
				tempCanvas.height = image.height;
			}

			// Initialize GL

			const gl = tempCanvas.getContext('webgl');
			var vertexShader = createShader(gl, gl.VERTEX_SHADER, VERTEX);
			var fragmentShader = createShader(gl, gl.FRAGMENT_SHADER, FRAGMENT);
			var program = createProgram(gl, vertexShader, fragmentShader);

			var positionLocation = gl.getAttribLocation(program, "a_position");
			var texcoordLocation = gl.getAttribLocation(program, "a_texCoord");
			var positionBuffer = gl.createBuffer();

			gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
			setRectangle(gl, 0, 0, image.width, image.height);
			var texcoordBuffer = gl.createBuffer();
			gl.bindBuffer(gl.ARRAY_BUFFER, texcoordBuffer);

			gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
				0.0, 0.0,
				1.0, 0.0,
				0.0, 1.0,
				0.0, 1.0,
				1.0, 0.0,
				1.0, 1.0,
			]), gl.STATIC_DRAW);

			var texture = gl.createTexture();
			gl.bindTexture(gl.TEXTURE_2D, texture);

			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
			gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);


			var resolutionLocation = gl.getUniformLocation(program, "u_resolution");
			gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
			gl.clearColor(0, 0, 0, 0);
			gl.clear(gl.COLOR_BUFFER_BIT);

			// initialize program
			gl.useProgram(program);

			for (let j = 1; j <= maskColorCount; j++) {
				const originalColor = originalMaskColors[`color${j}`];
				if (originalColor != null) { // original mask color at index
					const activeColor = activeMaskColors[`color${j}`];
					// const newColorInRGB = ColorControls.hexToRGB(activeColor); // active mask color at index

					var baseColor = hexToRgb("#" + originalColor);
					var color = [baseColor.r, baseColor.g, baseColor.b, 1.0];

					var newColor = hexToRgb("#" + activeColor);
					var replace = [newColor.r, newColor.g, newColor.b, 1.0];

					// change the variable
					var u_color = gl.getUniformLocation(program, `u_color${j}`);
					var u_replace = gl.getUniformLocation(program, `u_replace${j}`);
					gl.uniform4fv(u_color, color);
					gl.uniform4fv(u_replace, replace);

					var u_maskCount = gl.getUniformLocation(program, "u_maskCount");
					gl.uniform1i(u_maskCount, maskColorCount);
				}
			}

			gl.enableVertexAttribArray(positionLocation);
			gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

			// Tell the position attribute how to get data out of positionBuffer (ARRAY_BUFFER)
			var size = 2;          // 2 components per iteration
			var type = gl.FLOAT;   // the data is 32bit floats
			var normalize = false; // don't normalize the data
			var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
			var offset = 0;        // start at the beginning of the buffer
			gl.vertexAttribPointer(
				positionLocation, size, type, normalize, stride, offset);

			// Turn on the teccord attribute
			gl.enableVertexAttribArray(texcoordLocation);

			// Bind the position buffer.
			gl.bindBuffer(gl.ARRAY_BUFFER, texcoordBuffer);

			// Tell the position attribute how to get data out of positionBuffer (ARRAY_BUFFER)
			var size = 2;          // 2 components per iteration
			var type = gl.FLOAT;   // the data is 32bit floats
			var normalize = false; // don't normalize the data
			var stride = 0;        // 0 = move forward size * sizeof(type) each iteration to get the next position
			var offset = 0;        // start at the beginning of the buffer
			gl.vertexAttribPointer(
				texcoordLocation, size, type, normalize, stride, offset);

			// set the resolution
			gl.uniform2f(resolutionLocation, gl.canvas.width, gl.canvas.height);


			// Draw the rectangle.
			var primitiveType = gl.TRIANGLES;
			var offset = 0;
			var count = 6;
			gl.drawArrays(primitiveType, offset, count);

			return gl.canvas;
		}

		function setRectangle(gl, x, y, width, height) {
			var x1 = x;
			var x2 = x + width;
			var y1 = y;
			var y2 = y + height;
			gl.bufferData(gl.ARRAY_BUFFER, new Float32Array([
				x1, y1,
				x2, y1,
				x1, y2,
				x1, y2,
				x2, y1,
				x2, y2,
			]), gl.STATIC_DRAW);
		}

		function createShader(gl, type, source) {
			var shader = gl.createShader(type);
			gl.shaderSource(shader, source);
			gl.compileShader(shader);
			var success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
			if (success) {
				return shader;
			}

			// console.log(gl.getShaderInfoLog(shader));
			gl.deleteShader(shader);
		}

		function createProgram(gl, vertexShader, fragmentShader) {
			var program = gl.createProgram();
			gl.attachShader(program, vertexShader);
			gl.attachShader(program, fragmentShader);
			gl.linkProgram(program);
			var success = gl.getProgramParameter(program, gl.LINK_STATUS);
			if (success) {
				return program;
			}

			// console.log(gl.getProgramInfoLog(program));
			gl.deleteProgram(program);
		}

		function hexToRgb(hex) {
			var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
			return result ? {
				r: parseInt(result[1], 16) / 255,
				g: parseInt(result[2], 16) / 255,
				b: parseInt(result[3], 16) / 255
			} : null;
		}

		main();

		var glCanvas = main();
		return glCanvas;
	}

	private updateMaterialWithTextAndMask(model, type, glCanvas) {
		const mesh = SceneManager.getObjectByName(model, type);
		if (mesh) {
			if (mesh.material.map) {

				var meshTexture = new Texture(glCanvas);
				meshTexture.flipY = false;
				mesh.material.map = meshTexture;
				mesh.material.map.needsUpdate = true;

			}
		}
	}

	private createBlankCanvas(map: Texture) {

		const canvas: HTMLCanvasElement = document.createElement('canvas');
		canvas.width = map.image.width;
		canvas.height = map.image.height;
		const ctx: any = canvas.getContext('2d'); // <any> type on ctx, because IDE doesnt know putImageData exists on it and shows error

		return [canvas, ctx];
	}

	private getAllColorsOnActiveMask(type) {
		if (this._activeMaskColors[type]) return this._activeMaskColors[type];
		return [];
	}

	private getAllColorsOnOriginalMask(type) {
		if (this._originalMaskColors[type]) return this._originalMaskColors[type];
		return [];
	}

	private getOriginalMaskColor(type, index) {
		if (this._originalMaskColors[type] && this._originalMaskColors[type][index]) {
			return this._originalMaskColors[type][`color${index}`];
		}
		return '000000'; // black is not a color
	}

	private getActiveMaskColor(type, index) {
		if (this._activeMaskColors[type] && this._activeMaskColors[type][index]) {
			return this._activeMaskColors[type][`color${index}`];
		}
		return '000000'; // black is not a color
	}

	private setActiveMaskColor(type, color, index) {

		if (!this._activeMaskColors[type]) this._activeMaskColors[type] = {};
		this._activeMaskColors[type][`color${index}`] = color;
		return true; // done
	}

	private setOriginalMaskColor(type, color, index) {
		if (!this._originalMaskColors[type]) this._originalMaskColors[type] = {};

		this._originalMaskColors[type][`color${index}`] = color;

		return true; // done
	}

	private debugCanvas(id, canvas) {
		// const parent = document.getElementById(id);
		// parent.innerHTML = '';
		// parent.appendChild(canvas);
	}

	public swapColorsOnTexture = (map: Texture, color: string, maskType: string, index: number = 0) => {

		// console.log('-- SWAP color on texture', maskType, color, index);

		const indexPosition = index + 1; // because we start with 1 instead of 0 (previous dev did that, may want to update later because this creates confusion)

		// change active color
		this.setActiveMaskColor(maskType, color, indexPosition);

		// create canvas with default texture on it

		const [canvas, ctx] = this.createBlankCanvas(map);
		var originalImage = this.getOriginalImageDataFor(maskType);
		if (originalImage) ctx.putImageData(originalImage, 0, 0);

		const changedText = this.checkForAdditionalActiveTexts(null, canvas, ctx, this.getPositionRelation(maskType)); // 
		const changedTexture = this.replaceModifiedColorsWithShaders(canvas, ctx, maskType);
		this.updateMaterialWithTextAndMask(this._3DModel, maskType, changedTexture);
		
		if (changedTexture) { // if changes were made
			this.debugCanvas('debug1', canvas);
		}

	}

	private getStitchType(position) {

		const productConfig = this._main.product.getItemConfig();
		var arr = productConfig.customText;
		var att = "position";
		var stitch;


		var filterObjects = (obj, key, value) => obj.find(v => v[key] === value);
		var stitchType = filterObjects(arr, att, position);

		if (stitchType) stitch = stitchType.stitch;

		return stitch;
	}

	private checkForAdditionalActiveTexts(not, canvas, ctx, only: string = '') {

		// console.log('checkForAdditionalActiveTexts', not, only, this._activeCustomTexts);
		var stitchType;

		let changed = 0;
		if (Object.keys(this._activeCustomTexts).length > 0) {
			for (var position in this._activeCustomTexts) {
				if (only !== '') {

					if (position == only) {
						let config = this._activeCustomTexts[position];
						//console.log('-- change ONLY for', only, position, config, position == only, typeof position, typeof only);
						if (config && config.text.length > 0) {
							if (TEXT_STITCH_ENABLED) stitchType = this.getStitchType(config.position);
							// if (this._textColors[position]) config.config.color = this._textColors[position];
							this.drawText(config.text, config.config, canvas, ctx, stitchType);
							changed++;

						}
					} else {
						//console.log('-NOPE', position, only);
					}

				} else if (position != not) {
					let config = this._activeCustomTexts[position];
					if (config && config.text.length > 0) {
						if (TEXT_STITCH_ENABLED) stitchType = this.getStitchType(config.position);
						// if (this._textColors[position]) config.config.color = this._textColors[position];
						this.drawText(config.text, config.config, canvas, ctx, stitchType);
						changed++;
					}
				}
			}
		}

		return changed;
	}

	// when color is updated (text was previously added), it uses these values to determine if text should be redrawn
	private saveTextPlacement(maskType: string, position, text: string, config) {

		//console.log('--- saveTextPlacement ', maskType, position);

		this._activeCustomTexts[position] = {
			text: text,
			type: maskType,
			config: config, // color exists here & position too
		}


		//console.log('text placements', position, this._activeCustomTexts);
	}



	private countColorsForMask(colorsOnMask) {
		return Object.keys(colorsOnMask).length;
	}


	// if actual color code doesnt exist in color constants, look for closest color and return its color code
	public getMissingColor(colorCode) {



	}


}

