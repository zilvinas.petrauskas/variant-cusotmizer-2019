
export class NearestColor {

    private _colors = [];
    private _cache = {}; // for colors that already been fixed

	constructor(colorsArray) {
		this._colors = this.mapColors(colorsArray);
	}


	public find(color) {

        if(this._cache.hasOwnProperty(color)){
            return this._cache[color]; // return cached value (color already been found before)
        }

		return this.nearest(color, this._colors); // search for correct color code
	}

	private nearest(needle, colors) {

		needle = this.parseColor(needle);
		// console.log('1', needle);

		if (!needle) {
			return null;
		}

		var distanceSq,
			minDistanceSq = Infinity,
			rgb,
			value;

		for (var i = 0; i < colors.length; ++i) {
			rgb = colors[i].rgb;

			distanceSq = (
				Math.pow(needle.r - rgb.r, 2) +
				Math.pow(needle.g - rgb.g, 2) +
				Math.pow(needle.b - rgb.b, 2)
			);

			if (distanceSq < minDistanceSq) {
				minDistanceSq = distanceSq;
				value = colors[i];
			}
		}

		// if (value.name) {
		// 	return {
		// 		name: value.name,
		// 		value: value.source,
		// 		rgb: value.rgb,
		// 		distance: Math.sqrt(minDistanceSq)
		// 	};
		// }

        this._cache[needle] = value.source; // cache colors that were found once

		return value.source;
	}

	private parseColor(source) {

		var red, green, blue;

		if (typeof source === 'object') {
			return source;
		}

		var hexMatch = source.match(/^#?((?:[0-9a-f]{3}){1,2})$/i);
		if (hexMatch) {
			hexMatch = hexMatch[1];

			if (hexMatch.length === 3) {
				hexMatch = [
					hexMatch.charAt(0) + hexMatch.charAt(0),
					hexMatch.charAt(1) + hexMatch.charAt(1),
					hexMatch.charAt(2) + hexMatch.charAt(2)
				];

			} else {
				hexMatch = [
					hexMatch.substring(0, 2),
					hexMatch.substring(2, 4),
					hexMatch.substring(4, 6)
				];
			}

			red = parseInt(hexMatch[0], 16);
			green = parseInt(hexMatch[1], 16);
			blue = parseInt(hexMatch[2], 16);

			return { r: red, g: green, b: blue };
		}
	}

	private mapColors(colors) {
        const scope = this;
		if (colors instanceof Array) {
			return colors.map(function (color) {
				return scope.createColorSpec(color);
			});
		}

		return Object.keys(colors).map(function (name) {
			return scope.createColorSpec(colors[name], name);
		});
	}
	private createColorSpec(input, name?) {

		var color = <any>{};

		if (name) {
			color.name = name;
		}

		if (typeof input === 'string') {
			color.source = input;
			color.rgb = this.parseColor(input);

		} else if (typeof input === 'object') {
			// This is for if/when we're concatenating lists of colors.
			if (input.source) {
				return this.createColorSpec(input.source, input.name);
			}

			color.rgb = input;
			color.source = this.rgbToHex(input);
		}

		return color;
	}

	public rgbToHex(rgb) {
		return '#' + this.leadingZero(rgb.r.toString(16)) +
			this.leadingZero(rgb.g.toString(16)) + this.leadingZero(rgb.b.toString(16));
	}

	private leadingZero(value) {
		if (value.length === 1) {
			value = '0' + value;
		}
		return value;
	}
}